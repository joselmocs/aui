/*!
 * apps.aui.base.js
 * Base contém métodos úteis utilizados por várias páginas da
 * aplicação AUI.
 *
 * Joselmo Cardozo
 */

require(["libs/ajs/cookie"]);
require(["AJS", "App"], function(AJS, App) {
    AJS.log("apps.aui.base loaded")
});