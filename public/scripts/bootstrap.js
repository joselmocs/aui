/*!
 * bootstrap.js
 * Inicialização e configuração do requirejs e execução de alguns outros métodos
 * necessários para perfeito funcionamento.
 *
 * Joselmo Cardozo
 */

(function() {
    requirejs.config({
        baseUrl: "/scripts",
        paths: {
            jquery: "libs/jquery/jquery-1.8.3",
            AJS: "libs/ajs/atlassian",
            App: "libs/app/base"
        }
    });

    require([
        'jquery'
    ], function(jQuery) {
        jQuery.noConflict(true);
        require(["libs/html5-shiv/html5"]);
    });
})();
