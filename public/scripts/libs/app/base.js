/*!
 * app.base.js
 * App contém métodos úteis utilizados por várias partes do sistema.
 *
 * Joselmo Cardozo
 */

define([
    "AJS"
], function(AJS) {
    var App = (function () {
        var res = {
            context: []
        };
        var init = function() {
            if (typeof _context !== "undefined") {
                res.context = _context;
                _context = null;
            }
            AJS.log("app initialized");
        }();
        init = null;
        var result = {};
        for (var j in res) {
            result[j] = res[j];
        }
        return result;
    })();

    return App;
});