<?php

class BaseController extends Controller {

    /**
     * Context base enviado para a view.
     *
     * @var array
     */
    private $context = array(
        'template' => array(
            'field_errors' => array(),
            'form_errors' => array()
        ),
        'json' => array()
    );

    /**
     * Definimos alguns contexts globais que serão retornados em todas as
     * requisições.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Adiciona um array com variáveis que serão enviados à view para ser
     * renderizado pelo javascript.
     *
     * @param array $contexts
     */
    public function send_context_json($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['json'][$key] = $value;
        }
    }

    /**
     * Adiciona um array com variáveis que serão enviados à view.
     *
     * @param array $contexts
     */
    public function send_context($contexts = array())
    {
        foreach ($contexts as $key => $value) {
            $this->context['template'][$key] = $value;
        }
    }

    /**
     * Constroi a view e retorna o template.
     *
     * @param string $template
     * @return \Illuminate\View\View
     */
    public function view($template)
    {
        return View::make($template)
            ->with('json', json_encode($this->context['json']))
            ->with('context', (object) $this->context['template']);
    }
}