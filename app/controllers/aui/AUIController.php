<?php

class AUIController extends BaseController {

    /**
     * Exibe a página inicial AUI
     *
     * @return Illuminate\Support\Facades\View
     */
    public function index()
    {
        return $this->view('aui/index');
    }
}