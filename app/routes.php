<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('index');
});


/*
|--------------------------------------------------------------------------
| AUI Routes
|--------------------------------------------------------------------------
|
| Rotas da documentação/exemplos AUI
|
*/

Route::group(array('prefix' => 'aui'), function()
{
    Route::get('/', function() {
        return View::make('aui/index');
    });

    // Foundation Menu
    Route::group(array('prefix' => 'base'), function()
    {
        // Grid
        Route::get('grid', function() {
            return View::make('aui/base/grid');
        });

        // Layouts
        Route::group(array('prefix' => 'layout'), function()
        {
            Route::get('/', function() {
                return View::make('aui/base/layout');
            });
            Route::get('fixed', function() {
                return View::make('aui/base/examples/layout/page-layout')
                    ->with('name', "Fixed")
                    ->with('class', 'aui-page-fixed');
            });
            Route::get('fluid', function() {
                return View::make('aui/base/examples/layout/page-layout')
                    ->with('name', "Fluid")
                    ->with('class', 'aui-page-fluid');
            });
            Route::get('hybrid', function() {
                return View::make('aui/base/examples/layout/page-layout')
                    ->with('name', "Hybrid")
                    ->with('class', 'aui-page-hybrid');
            });
            Route::get('content', function() {
                return View::make('aui/base/examples/layout/content');
            });
            Route::get('nav-content', function() {
                return View::make('aui/base/examples/layout/nav-content');
            });
            Route::get('content-sidebar', function() {
                return View::make('aui/base/examples/layout/content-sidebar');
            });
            Route::get('nav-content-sidebar', function() {
                return View::make('aui/base/examples/layout/nav-content-sidebar');
            });
            Route::get('focused-task', function() {
                return View::make('aui/base/examples/layout/focused-task');
            });
        });

        // Typography
        Route::get('typography', function() {
            return View::make('aui/base/typography');
        });

        // Colors
        Route::get('colors', function() {
            return View::make('aui/base/colors');
        });

        // Iconography
        Route::get('iconography', function() {
            return View::make('aui/base/iconography');
        });

        // Avatars
        Route::get('avatars', function() {
            return View::make('aui/base/avatars');
        });
    });

    // Controls Menu
    Route::group(array('prefix' => 'controls'), function()
    {
        // Buttons
        Route::get('buttons', function() {
            return View::make('aui/controls/buttons');
        });

        // Forms
        Route::get('forms', function() {
            return View::make('aui/controls/forms');
        });

        // Tables
        Route::get('tables', function() {
            return View::make('aui/controls/tables');
        });

        // Tabs
        Route::get('tabs', function() {
            return View::make('aui/controls/tabs');
        });

        // Toolbar
        Route::get('toolbar', function() {
            return View::make('aui/controls/toolbar');
        });
    });

});