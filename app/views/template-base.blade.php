<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=EDGE" http-equiv="X-UA-Compatible">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>AUI</title>
    {{ HTML::style("/styles/aui/aui-reset.css") }}
    {{ HTML::style("/styles/aui/html5.css") }}
    {{ HTML::style("/styles/aui/basic.css") }}
    {{ HTML::style("/styles/aui/aui-page-typography.css") }}
    {{ HTML::style("/styles/aui/aui-page-layout.css") }}
    @section('styles')
    @show

</head>
<body class="@yield('aui-page-type')">

@yield('header')
@yield('content')
@yield('footer')

<script data-main="/scripts/bootstrap" src="/scripts/libs/requirejs/require-2.1.8.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
@if (isset($json))
var _context = {{ $json }};
@else
var _context = [];
@endif
@section('javascript-cdata')
@show
//]]>
</script>
@section('scripts')
@show

</body>
</html>