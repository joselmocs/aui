@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-lozenge.css") }}
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("javascript-cdata")@parent
require(["apps/aui/base"]);
@endsection

@section("content")
<section id="content">
    <header class="aui-page-header aui-page-header-hero">
        <div class="aui-page-header-inner">
            <div class="aui-page-header-main">
                <h1>AUI Documentação (exemplos)<span class="aui-lozenge aui-lozenge-success">1.2</span></h1>
                <p>Princípios de design, componentes, padrões e diretrizes para a construção de layouts AUI.</p>
            </div>
        </div>
    </header>
</section>
@endsection