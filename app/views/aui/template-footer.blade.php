@section('footer')
<footer id="footer">
    <section class="footer-body">
        <ul id="aui-footer-list">
            <li>Copyright &copy; 2013 AUI</li>
            <li><a href="#">Notas da versão</a></li>
            <li><a href="#">Relatar um problema</a></li>
        </ul>
    </section>
</footer><!-- #footer -->
@endsection