@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/tables.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-group.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">
    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Foundation</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Avatars</h2>

    <p>Avatars are used to add a human touch and instant clarity when understanding which user did what in the application. We also use avatars for projects, repositories, spaces and other container metaphors within Atlassian Apps.</p>

    <h3>User avatars</h3>
    <p>User avatars come in 5 sizes. All user avatars have a border-radius on them. No sharp edges, please.</p>

    <table class="aui">
        <thead>
        <tr>
            <th id="basic-size" style="width:60px">Size</th>
            <th id="basic-example" style="width:140px">Example</th>
            <th id="basic-radius" style="width:140px">Border radius</th>
            <th id="basic-description">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td headers="basic-size">16px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-xsmall">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/avatar-16.png" alt="Username">
                    </div>
                </div>
            </td>
            <td headers="basic-radius">3px</td>
            <td headers="basic-description">Only use this size avatar when screen real estate is at an absolute premium e.g. A dropdown menu that makes 24px avatars look too squished</td>
        </tr>
        <tr>
            <td headers="basic-size">24px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-small">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/avatar-24.png" alt="Username">
                    </div>
                </div>
            </td>
            <td headers="basic-radius">3px</td>
            <td headers="basic-description">Use this size when the user appears as metadata on the screen e.g. Meta data on a JIRA issue</td>
        </tr>
        <tr>
            <td headers="basic-size">32px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-medium">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/avatar-32.png" alt="Username">
                    </div>
                </div>
            </td>
            <td headers="basic-radius">3px</td>
            <td headers="basic-description">Primarily used as a statement avatar e.g. Activity streams and comments</td>
        </tr>
        <tr>
            <td headers="basic-size">48px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-large">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/avatar-48.png" alt="Username">
                    </div>
                </div>
            </td>
            <td headers="basic-radius">3px</td>
            <td headers="basic-description">Use this size for main page titles e.g. On the account settings for a user</td>
        </tr>
        <tr>
            <td headers="basic-size">96px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-xxlarge">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/avatar-96.png" alt="Username">
                    </div>
                </div>
            </td>
            <td headers="basic-radius">5px</td>
            <td headers="basic-description">Used for the hero element e.g. User profiles or info panels on hover</td>
        </tr>
        </tbody>
    </table>

    <h3>Project avatars</h3>
    <p>Avatars are used for projects, spaces and repositories to give them a visual identity. These avatars are round to clearly differentiate them from user avatars.</p>

    <table class="aui">
        <thead>
        <tr>
            <th id="basic-size" style="width:60px">Size</th>
            <th id="basic-example" style="width:140px">Example on white</th>
            <th id="basic-example" style="width:140px;">Example on grey</th>
            <th id="basic-description">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td headers="basic-size">24px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-small aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-24.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-example" class="inverted-background">
                <div class="aui-avatar aui-avatar-small aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-24.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-description">General used when room is tight in the UI. Acceptable to use in menus</td>
        </tr>
        <tr>
            <td headers="basic-size">32px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-32.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-example" class="inverted-background">
                <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-32.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-description">Used in table views for project listings</td>
        </tr>
        <tr>
            <td headers="basic-size">48px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-large aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-48.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-example" class="inverted-background">
                <div class="aui-avatar aui-avatar-large aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-48.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-description">Used in sidebars or a compacted version of the <a href="page-header.html">Page header</a> where the 64px variation does not fit</td>
        </tr>
        <tr>
            <td headers="basic-size">64px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-64.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-example" class="inverted-background">
                <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-64.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-description">Primarily used in the <a href="page-header.html">Page header</a> for identifying the current entity</td>
        </tr>
        <tr>
            <td headers="basic-size">128px</td>
            <td headers="basic-example">
                <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-128.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-example" class="inverted-background">
                <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                    <div class="aui-avatar-inner">
                        <img src="/images/aui/examples/avatars/project-128.png" alt="Project avatar">
                    </div>
                </div>
            </td>
            <td headers="basic-description">Used in overview pages where avatars are used in a card view</td>
        </tr>
        </tbody>
    </table>

    <h3>Default avatars</h3>
    <p>Default avatars are provided in products if the user does not upload a customised version.</p>

    <h4>Users</h4>
    <table class="aui">
    <thead>
    <tr>
        <th id="basic-size" style="max-width:50px">Size</th>
        <th id="blue-example">Blue</th>
        <th id="cyan-example">Cyan</th>
        <th id="green-example">Green</th>
        <th id="yellow-example">Yellow</th>
        <th id="orange-example">Orange</th>
        <th id="red-example">Red</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-size">16px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-blue-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-cyan-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-green-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-yellow-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-orange-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-red-16@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">24px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-blue-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-cyan-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-green-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-yellow-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-orange-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-red-24@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">32px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-blue-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-cyan-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-green-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-yellow-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-orange-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-red-32@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">48px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-blue-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-cyan-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-green-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-yellow-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-orange-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-red-48@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">96px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-blue-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-cyan-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-green-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-yellow-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-orange-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-user/user-avatar-red-96@2x.png" alt="User Avatar">
                </div>
            </div>
        </td>
    </tr>
    </tbody>
    </table>

    <h4>Groups</h4>
    <table class="aui">
    <thead>
    <tr>
        <th id="basic-size" style="max-width:50px">Size</th>
        <th id="blue-example">Blue</th>
        <th id="cyan-example">Cyan</th>
        <th id="green-example">Green</th>
        <th id="yellow-example">Yellow</th>
        <th id="orange-example">Orange</th>
        <th id="red-example">Red</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-size">16px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-blue-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-cyan-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-green-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-yellow-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-orange-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xsmall">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-red-16@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">24px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-blue-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-cyan-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-green-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-yellow-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-orange-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-small">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-red-24@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">32px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-blue-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-cyan-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-green-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-yellow-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-orange-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-medium">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-red-32@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">48px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-blue-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-cyan-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-green-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-yellow-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-orange-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-large">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-red-48@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">96px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-blue-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-cyan-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-green-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-yellow-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-orange-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xxlarge">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-group/group-avatar-red-96@2x.png" alt="Group Avatar">
                </div>
            </div>
        </td>
    </tr>
    </tbody>
    </table>

    <h4>Projects</h4>
    <table class="aui">
    <thead>
    <tr>
        <th id="basic-size" style="max-width:50px">Size</th>
        <th id="blue-example">Blue</th>
        <th id="cyan-example">Cyan</th>
        <th id="green-example">Green</th>
        <th id="yellow-example">Yellow</th>
        <th id="orange-example">Orange</th>
        <th id="red-example">Red</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-size">24px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-blue-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-cyan-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-green-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-yellow-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-orange-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-small aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-red-24@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">32px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-blue-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-cyan-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-green-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-yellow-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-orange-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-medium aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-red-32@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">48px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-blue-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-cyan-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-green-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-yellow-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-orange-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-large aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-red-48@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">64px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-blue-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-cyan-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-green-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-yellow-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-orange-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-red-64@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td headers="basic-size">128px</td>
        <td headers="blue-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-blue-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="cyan-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-cyan-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="green-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-green-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="yellow-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-yellow-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="orange-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-orange-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
        <td headers="red-example">
            <div class="aui-avatar aui-avatar-xxxlarge aui-avatar-project">
                <div class="aui-avatar-inner">
                    <img src="/images/aui/avatars/default-project/project-avatar-red-128@2x.png" alt="Project Avatar">
                </div>
            </div>
        </td>
    </tr>
    </tbody>
    </table>

    <h3>Contextual examples</h3>
    <div class="example-thumbs">
        <a href="/images/aui/examples/avatars/example-full-01.png" target="_blank"><img src="/images/aui/examples/avatars/example-thumb-01.png"></a>
        <a href="/images/aui/examples/avatars/example-full-02.png" target="_blank"><img src="/images/aui/examples/avatars/example-thumb-02.png"></a>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/avatars.html" target="_blank">AUI Documentation</a> – implementation details</li>
        <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=avatars" target="_blank">AUI Sandbox</a> – code snippets</li>
    </ul>
    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection