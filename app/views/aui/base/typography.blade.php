@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/tables.css") }}
    {{ HTML::style("/styles/aui/aui-lozenge.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Foundation</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Typography</h2>

    <p>Typography is the cornerstone of our design language and follows our <a href="principles.html">guiding principles</a>. With our "just enough is more" design principle, we rely heavily on typography, not only as the means of written communication but as the user interface itself. A strong grounding in typography gives us the foundation upon which we can build out other components.</p>

    <h3>Typographic elements</h3>
    <table class="aui">
        <thead>
        <tr>
            <th width="330">Element</th>
            <th width="140">Pixel sizes</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="type-element"><h1>Page Title</h1></td>
            <td><h1>24px</h1></td>
            <td>The main page heading is an <span class="aui-lozenge aui-lozenge-code">&lt;h1&gt;</span> element. You should only use it once per page</td>
        </tr>
        <tr>
            <td class="type-element"><h2>Heading</h2></td>
            <td><h2>20px</h2></td>
            <td>The secondary heading to the left is an <span class="aui-lozenge aui-lozenge-code">&lt;h2&gt;</span> element, which may be used as a page-level heading. More than one may be used per page</td>
        </tr>
        <tr>
            <td class="type-element"><h3>Third-level heading</h3></td>
            <td><h3>16px bold</h3></td>
            <td>The heading to the left is an <span class="aui-lozenge aui-lozenge-code">&lt;h3&gt;</span> element, which may be used after an <span class="aui-lozenge aui-lozenge-code">&lt;h2&gt;</span> heading in the document hierarchy</td>
        </tr>
        <tr>
            <td class="type-element"><h4>Fourth-level heading</h4></td>
            <td><h4>14px bold</h4></td>
            <td>The heading to the left is an<span class="aui-lozenge aui-lozenge-code">&lt;h4&gt;</span> element, which may be used after an <span class="aui-lozenge aui-lozenge-code">&lt;h3&gt;</span> heading in the document hierarchy</td>
        </tr>
        <tr>
            <td class="type-element"><h5>Fifth-level heading</h5></td>
            <td><h5>12px uppercase bold</h5></td>
            <td>The heading to the left is an <span class="aui-lozenge aui-lozenge-code">&lt;h5&gt;</span> element, which may be used after an <span class="aui-lozenge aui-lozenge-code">&lt;h4&gt;</span> heading in the document hierarchy</td>
        </tr>
        <tr>
            <td class="type-element"><h6>Sixth-level heading</h6></td>
            <td><h6>12px bold</h6></td>
            <td>The heading to the left is an <span class="aui-lozenge aui-lozenge-code">&lt;h6&gt;</span> element, which may be used after an <span class="aui-lozenge aui-lozenge-code">&lt;h5&gt;</span> heading in the document hierarchy</td>
        </tr>
        <tr>
            <td>This is a paragraph, using the default san-serif font, with 14px font size and 20px line height.</td>
            <td>14px</td>
            <td>
                <p>Within our guidelines the paragraph <span class="aui-lozenge aui-lozenge-code">&lt;p&gt;</span> demonstrates the generic font, color, and vertical rhythm. The paragraph is composed with a line height of 20px.</p>
                <p>This value is known as baseline, as it sets the distance between each line where the text "sits". The 20px value is our ideal vertical rhythm, but for general purposes we derived from it a grid of 10px to be used through out our design language. Doing so establishes a consistent tone for the user interface and increases the probability of elements sharing the same alignment/baseline even if set within different columns and/or contexts.</p>
            </td>
        </tr>
        <tr>
            <td class="type-element"><strong>Bold text</strong></td>
            <td>14px</td>
            <td>Use the <span class="aui-lozenge aui-lozenge-code">&lt;strong&gt;</span> for <strong>bolding</strong> text</td>
        </tr>
        <tr>
            <td class="type-element"><em>Italic text</em></td>
            <td>14px</td>
            <td>Use the <span class="aui-lozenge aui-lozenge-code">&lt;em&gt;</span> for <em>italic</em> text</td>
        </tr>
        <tr>
            <td class="type-element"><small>Small text</small></td>
            <td>12px</td>
            <td>Small text <span class="aui-lozenge aui-lozenge-code">&lt;small&gt;</span> is used primarily as help text under form fields and as secondary supporting text in applications. Use sparingly.</td>
        </tr>
        <tr>
            <td class="type-element"><a href="#">Hyperlink</a></td>
            <td>14px</td>
            <td></td>
        </tr>
        <tr>
            <td class="type-element">
                <ol>
                    <li>This is an ordered list</li>
                    <li>With three list items</li>
                    <li>As a possible example</li>
                </ol>
            </td>
            <td>14px</td>
            <td>Use ordered lists to semantically group items in a pre-determined order</td>
        </tr>
        <tr>
            <td class="type-element">
                <ul>
                    <li>This is an unordered list</li>
                    <li>With three list items</li>
                    <li>As a possible example</li>
                </ul>
            </td>
            <td>14px</td>
            <td>Use unordered lists to semantically group related items</td>
        </tr>
        <tr>
            <td class="type-element">
                <blockquote>
                    <p>This is a blockquote</p>
                </blockquote>
            </td>
            <td>14px</td>
            <td></td>
        </tr>
        <tr>
            <td><q>This is a an inline quote</q></td>
            <td>14px</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <pre><code>&lt;div class="foo"&gt;<br />&lt;h1&gt;Example code snippet&lt;/h1&gt;<br />&lt;/div&gt;</code>
                </pre>
            </td>
            <td>12px</td>
            <td>Used for displaying inline code blocks</td>
        </tr>
        </tbody>
    </table>

    <h3>Special typographic styles</h3>
    <p>Special typographic styles are an additional set of typographic elements designed to be used sparingly within products for in-product marketing, in-product education and blank slates.
        They are designed to be the most visually prominent elements on any given screen. </p>

    <p>Two sets have been created, comprising of a title and a description. Only one set should be used per context.</p>

    <table class="aui">
        <thead>
        <tr>
            <th width="330">Element</th>
            <th width="140">Pixel sizes</th>
            <th>Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="type-element">
                <header class="aui-page-header aui-page-header-hero">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1 style="margin-top:0;">Hero Header</h1>
                        </div>
                    </div>
                </header>
            </td>
            <td>
                <header class="aui-page-header aui-page-header-hero">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1 style="margin-top:0;">48px</h1>
                        </div>
                    </div>
                </header>
            </td>
            <td>This title should be used sparingly for special introductory areas such as landing pages</td>
        </tr>
        <tr>
            <td class="type-element">
                <header class="aui-page-header aui-page-header-hero">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <p style="padding-bottom:0; margin-top:0;">Hero description</p>
                        </div>
                    </div>
                </header>
            </td>
            <td>
                <header class="aui-page-header aui-page-header-hero">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <p style="padding-bottom:0; margin-top:0;">24px</p>
                        </div>
                    </div>
                </header>
            </td>
            <td>This description can be used with the hero title for accompanying explanatory content. It can only be used after the hero title.</td>
        </tr>
        <tr>
            <td class="type-element">
                <header class="aui-page-header aui-page-header-marketing">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h2 style="margin-top:0;">Marketing area title</h2>
                        </div>
                    </div>
                </header>
            </td>
            <td>
                <header class="aui-page-header aui-page-header-marketing">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h2 style="margin-top:0;">36px</h2>
                        </div>
                    </div>
                </header>
            </td>
            <td>This title should be used when marketing messaging or onboarding content is required.</td>
        </tr>
        <tr>
            <td class="type-element">
                <header class="aui-page-header aui-page-header-marketing">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <p style="margin-top:0;">Marketing area description
                            </p></div>
                    </div>
                </header>
            </td>
            <td>
                <header class="aui-page-header aui-page-header-marketing">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <p style="margin-top:0;">20px
                            </p></div>
                    </div>
                </header>
            </td>
            <td>This description can be used with the marketing area title for accompanying explanatory content. It can only be used after the marketing area title.</td>
        </tr>
        </tbody>
    </table>

    <h3>Usage</h3>
    <h4>Do</h4>
    <ul>
        <li>Establish a visual and semantic hierarchy by nesting headings correctly</li>
        <li>For example, an <span class="aui-lozenge aui-lozenge-code">&lt;h3&gt;</span> follows an <span class="aui-lozenge aui-lozenge-code">&lt;h2&gt;</span> when it denotes a section contained within the <span class="aui-lozenge aui-lozenge-code">&lt;h2&gt;</span>'s content. Don't skip to an <span class="aui-lozenge aui-lozenge-code">&lt;h4&gt;</span> after an <span class="aui-lozenge aui-lozenge-code">&lt;h2&gt;</span>, etc</li>
        <li>Position text on the baseline grid where practical</li>
    </ul>

    <h4>Don't</h4>
    <ul>
        <li>Use headings arbitrarily based on their presentation</li>
    </ul>

    <h3>Using hyperlinks</h3>
    <p>Unless explicitly specified by another component in the Atlassian Design Guidelines, hyperlinks will be the standard hyperlink color <span class="aui-lozenge aui-lozenge-code">#3b73af</span> and not underlined except on hover.</p>
    <p>In the case of user generated content, a user's semantic structure choice trumps their presentational choice. Thus, the default hyperlink treatment outweighs a color applied to the containing context of a link. If the user has the ability to apply a color choice directly to a link, and only to the link, then the product may consider respecting the user's color choice.</p>

    <h3>Line lengths and readability</h3>
    <p>It is recommended that products should aim for a maximum line length of 100 characters per line (including spaces) under normal circumstances.</p>
    <p>As users have control over the layout width in most cases (fluid layout), a line length can not always be specified. We can, however, design for an ideal line length range under normal circumstances and use responsive design techniques to anticipate different viewport contexts.</p>

    <h3>Contextual examples</h3>
    <div class="example-thumbs">
        <a href="/images/aui/examples/typography/example-full-01.png" target="_blank"><img src="/images/aui/examples/typography/example-thumb-01.png" alt=""></a>
        <a href="/images/aui/examples/typography/example-full-02.png" target="_blank"><img src="/images/aui/examples/typography/example-thumb-02.png" alt=""></a>
        <a href="/images/aui/examples/typography/example-full-03.png" target="_blank"><img src="/images/aui/examples/typography/example-thumb-03.png" alt=""></a>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/typography.html" target="_blank">AUI Documentation</a> – implementation details</li>
    </ul>
    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection