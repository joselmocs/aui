@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-group.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Foundation</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Iconography</h2>

    <p>Icons are visual artifacts to communicate, guide and inform the user. They can help users find their way through an interface, make the meaning of buttons clearer, and prepare users for what is happening next. Icons work at their best when they're consistent, familiar and recognizable to the user.</p>

    <h3>When and how to use icons</h3>
    <p>An icon collection functions as a system and each new addition needs to be checked against the existing icon set. If choosing an existing icon, consider where it is currently used and how it compares metaphorically. Avoid repurposing an existing icon for a new/different action.</p>
    <p>Icons can be used to:</p>
    <ul>
        <li>Reference an existing content type within an application, for example a JIRA issue or a Confluence page</li>
        <li>Facilitate understanding of UI elements, distinguishing them and communicating its purpose within the overall interface or user flow</li>
        <li>Communicate a secondary status, for example the priority of a JIRA issue. For a primary status <a href="lozenges.html">Lozenges</a> are the recommended pattern</li>
    </ul>
    <p>When applying an icon to an existing component, think about how it will fit within existing patterns. Currently these components have guidelines on how to include icons:</p>
    <ul>
        <li><a href="application-header.html">Application header</a></li>
        <li><a href="buttons.html">Buttons</a></li>
        <li><a href="messages.html">Messages</a></li>
    </ul>

    <h3>The Atlassian icon font</h3>
    <p>Delivering icons as images presents challenges when they need to scale or change color. Therefore, Atlassian has began the process of moving the icon library to an 'icon font'. Icons that are delivered by the icon font in AUI are available in small (16px) and large (32px) sizes by default. We recommend not using this set of icons at 24px due to the poor rendering that will occur when scaled up to be 1.5x the size of the original 16px. The initial set composes a minimum range, with future updates to expand the set and add new icons.</p>

    <div class="aui-group" id="icon-font-table">
    <div class="aui-item icon-size-examples">
        <h4>Icon sizes</h4>
        <div class="icon-size-examples-small">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" original-title="">Configure</span> 16px
        </div>
        <div class="icon-size-examples-large">
            <span class="aui-icon aui-icon-large aui-iconfont-configure">Configure</span> 32px
        </div>
    </div>
    <div class="aui-item">
    <h4>Icons</h4>
    <div class="aui-group">
    <div class="aui-item">
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-add" data-unicode="UTF+E002" original-title="">Add</span> <small>Add</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-add-comment" data-unicode="UTF+E00B" original-title="">Add comment</span> <small>Add comment</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-approve" data-unicode="UTF+E013" original-title="">Approve</span> <small>Approve</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-appswitcher" data-unicode="UTF+E009" original-title="">Appswitcher</span> <small>App switcher</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-arrows-up" data-unicode="UTF+E02A" original-title="">Arrows up</span> <small>Arrows up</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-arrows-down" data-unicode="UTF+E02B" original-title="">Arrows down</span> <small>Arrows down</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-arrow-left" data-unicode="UTF+EC10" original-title="">Arrow left</span> <small>Arrow left</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-arrow-right" data-unicode="UTF+EC0F" original-title="">Arrow right</span> <small>Arrow right</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-attachment" data-unicode="UTF+E027" original-title="">Attachment</span> <small>Attachment</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-file-binary" data-unicode="UTF+EC03" original-title="">Binary</span> <small>Binary</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch" data-unicode="UTF+EC04" original-title="">Branch</span> <small>Branch</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-branch-small" data-unicode="UTF+EC14" original-title="">Branch small</span> <small>Branch small</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-browse-up" data-unicode="UTF+EC0B" original-title="">Browse up</span> <small>Browse up</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-build" data-unicode="UTF+E016" original-title="">Build</span> <small>Build</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-task-cancelled" data-unicode="UTF+EC12" original-title="">Build cancelled</span> <small>Build cancelled</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-task-disabled" data-unicode="UTF+EC13" original-title="">Build disabled</span> <small>Build disabled</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-task-in-progress" data-unicode="UTF+EC11" original-title="">Build progress</span> <small>Build progress</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-calendar" data-unicode="UTF+E028" original-title="">Calendar</span> <small>Calendar</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-checkout" data-unicode="UTF+EC17" original-title="">Check out</span> <small>Check out</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-compare" data-unicode="UTF+EC16" original-title="">Compare</span> <small>Compare</small>
        </div>
    </div>
    <div class="aui-item">
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-jira-completed-task" data-unicode="UTF+E800" original-title="">Completed task</span> <small>Completed task</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-configure" data-unicode="UTF+E001" original-title="">Configure</span> <small>Configure</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-clone" data-unicode="UTF+EC0C" original-title="">Clone</span> <small>Clone</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-close-dialog" data-unicode="UTF+E00E" original-title="">Close dialog</span> <small>Close dialog</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-collapsed" data-unicode="UTF+E024" original-title="">Collapsed</span> <small>Collapsed</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-comment" data-unicode="UTF+E00A" original-title="">Comment</span> <small>Comment</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-file-commented" data-unicode="UTF+EC15" original-title="">Commented file</span> <small>Commented file</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-commit" data-unicode="UTF+EC08" original-title="">Commit</span> <small>Commit</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-confluence" data-unicode="UTF+E026" original-title="">Confluence</span> <small>Confluence</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-details" data-unicode="UTF+E029" original-title="">Details</span> <small>Details</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-doc" data-unicode="UTF+E019" original-title="">Document</span> <small>Document</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-edit" data-unicode="UTF+E014" original-title="">Edit</span> <small>Edit</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-email" data-unicode="UTF+E008" original-title="">Email</span> <small>Email</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-error" data-unicode="UTF+E011" original-title="">Error</span> <small>Error</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-expanded" data-unicode="UTF+E023" original-title="">Expanded</span> <small>Expanded</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-file" data-unicode="UTF+EC02" original-title="">File</span> <small>File</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-flag" data-unicode="UTF+E025" original-title="">Flag</span> <small>Flag</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-folder-closed" data-unicode="UTF+EC00" original-title="">Folder</span> <small>Folder</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-folder-open" data-unicode="UTF+EC01" original-title="">Folder open</span> <small>Folder open</small>
        </div>
    </div>
    <div class="aui-item">
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-fork" data-unicode="UTF+EC0D" original-title="">Fork</span> <small>Fork</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-help" data-unicode="UTF+E003" original-title="">Help</span> <small>Help</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-info" data-unicode="UTF+E012" original-title="">Info</span> <small>Info</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-jira" data-unicode="UTF+E018" original-title="">JIRA</span> <small>JIRA</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-link" data-unicode="UTF+E02C" original-title="">Link</span> <small>Link</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-locked" data-unicode="UTF+E02D" original-title="">Locked</span> <small>Locked</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-more" data-unicode="UTF+E02F" original-title="">More</span> <small>More</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-pull-request" data-unicode="UTF+EC09" original-title="">Pull request</span> <small>Pull request</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-quote" data-unicode="UTF+E030" original-title="">Quote</span> <small>Quote</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-remove" data-unicode="UTF+E00D" original-title="">Remove</span> <small>Remove</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-remove-label" data-unicode="UTF+E00F" original-title="">Remove label</span> <small>Remove label</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-repository" data-unicode="UTF+EC05" original-title="">Repository</span> <small>Repository</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-repository-forked" data-unicode="UTF+EC06" original-title="">Repository forked</span> <small>Repository forked</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-repository-locked" data-unicode="UTF+E007" original-title="">Repository locked</span> <small>Repository locked</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-search" data-unicode="UTF+E004" original-title="">Search</span> <small>Search</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-rss" data-unicode="UTF+E022" original-title="">RSS</span> <small>RSS</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-search-small" data-unicode="UTF+E00C" original-title=""></span> <small>Search small</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-share" data-unicode="UTF+E015" original-title="">Share</span> <small>Share</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-side-diff" data-unicode="UTF+EC19" original-title="">Side-by-side diff</span> <small>Side-by-side diff</small>
        </div>
    </div>
    <div class="aui-item">
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-star" data-unicode="UTF+E01A" original-title="">Star</span> <small>Star</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-submodule" data-unicode="UTF+EC0E" original-title="">Submodule</span> <small>Submodule</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-success" data-unicode="UTF+E005" original-title="">Success</span> <small>Success</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-tag" data-unicode="UTF+EC0A" original-title="">Tag</span> <small>Tag</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-devtools-tag-small" data-unicode="UTF+EC18" original-title="">Tag</span> <small>Tag small</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-jira-test-session" data-unicode="UTF+E801" original-title="">Test session</span> <small>Test session</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-time" data-unicode="UTF+E010" original-title="">Time</span> <small>Time</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-unlocked" data-unicode="UTF+E02E" original-title="">Unlocked</span> <small>Unlocked</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-unstar" data-unicode="UTF+E01B" original-title="">Unstar</span> <small>Unstar</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-unwatch" data-unicode="UTF+E01E" original-title="">Unwatch</span> <small>Unwatch</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-user" data-unicode="UTF+E017" original-title="">User</span> <small>User</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-view" data-unicode="UTF+E01C" original-title="">View</span> <small>View</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-view-card" data-unicode="UTF+E020" original-title="">View card</span> <small>View card</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-view-list" data-unicode="UTF+E01F" original-title="">View list</span> <small>View list</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-view-table" data-unicode="UTF+E021" original-title="">View table</span> <small>View table</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-warning" data-unicode="UTF+E031" original-title="">Warning</span> <small>Warning</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-watch" data-unicode="UTF+E01D" original-title="">Watch</span> <small>Watch</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-workbox" data-unicode="UTF+E006" original-title="">Workbox</span> <small>Workbox</small>
        </div>
        <div class="aui-icon-table">
            <span class="aui-icon aui-icon-small aui-iconfont-workbox-empty" data-unicode="UTF+E007" original-title="">Workbox empty</span> <small>Workbox empty</small>
        </div>
    </div>
    </div>
    </div>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/icons.html" target="_blank">AUI Documentation</a> – implementation details</li>
        <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=icons" target="_blank">AUI Sandbox</a> – code snippets</li>
    </ul>
    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection