@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-group.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Foundation</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <h2>Grid</h2>

                <p>The grid provides the foundation for a harmonious positioning of elements, while maintaining a consistent and coherent look of a page as a whole. It brings calm to a page, and makes it easier for users to scan the content.</p>
                <p>In HTML, text flows in a vertical motion, and this creates the basic rhythm of our pages. This rhythm is created by the space between each line of text, commonly named 'line height'. In visual terms, each line of text defines a visual guideline (the text literally 'sits' on it). This line is called 'baseline', and will be our common denominator in defining the grid.</p>

                <h3>Vertical best practices</h3>
                <ul>
                    <li>The minimum vertical unit is 10px</li>
                    <li>The 'baseline' is equivalent to 2 vertical units, i.e. 20px</li>
                    <li>When positioning elements vertically, try using multiples of 10px</li>
                    <li>Whenever possible, make sure that your objects line up, both vertically and horizontally</li>
                </ul>

                <h3>Horizontal best practices</h3>
                <ul>
                    <li>Horizontally distributed elements should be set 20px apart</li>
                    <li>Whenever possible, use AUI containers to position your elements</li>
                    <li>Avoid making anything just one column wide</li>
                    <li>Whenever possible, make sure that your objects line up, both vertically and horizontally</li>
                </ul>

                <h3>Interactive example</h3>
                <div class="example-container">
                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>one</p>
                        </div>
                    </div>

                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>two</p>
                        </div>
                        <div class="aui-item">
                            <p>two</p>
                        </div>
                    </div>

                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>three</p>
                        </div>
                        <div class="aui-item">
                            <p>three</p>
                        </div>
                        <div class="aui-item">
                            <p>three</p>
                        </div>
                    </div>

                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>four</p>
                        </div>
                        <div class="aui-item">
                            <p>four</p>
                        </div>
                        <div class="aui-item">
                            <p>four</p>
                        </div>
                        <div class="aui-item">
                            <p>four</p>
                        </div>
                    </div>
                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                        <div class="aui-item">
                            <p>six</p>
                        </div>
                    </div>
                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                        <div class="aui-item">
                            <p>eight</p>
                        </div>
                    </div>
                    <div class="aui-group aui-group-highlighted">
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                        <div class="aui-item">
                            <p>twelve</p>
                        </div>
                    </div>
                </div>

                <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
                <ul>
                    <li><a href="http://docs.atlassian.com/aui/5.2/docs/layout.html" target="_blank">AUI Documentation</a> – implementation details</li>
                </ul>

            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection