@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/forms.css") }}
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default aui-page-focused aui-page-focused-medium @endsection

{{--
Variações do tamanho do bloco:
    aui-page-focused-small
    aui-page-focused-medium
    aui-page-focused-large
--}}

@section("content")
<section id="content" role="main">
    <div class="aui-page-panel margin-fix">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <h2>Focused task layout</h2>
                <form action="#" method="post" class="aui">
                    <fieldset>
                        <div class="field-group">
                            <label for="d-fname">Project name<span class="aui-icon icon-required"> required</span></label>
                            <input class="text" type="text" id="d-fname" name="d-fname" title="first name">
                        </div>
                        <div class="field-group">
                            <label for="d-fname">Key<span class="aui-icon icon-required"> required</span></label>
                            <input class="text short-field" type="text" id="d-fname" name="d-fname" title="first name">
                            <div class="description">Eg. AT (for a project named Atlassian)</div>
                        </div>
                        <div class="field-group">
                            <label for="comment">Description</label>
                            <textarea class="textarea long-field" name="comment" id="comment" rows="3" placeholder="Your project description…"></textarea>
                        </div>
                    </fieldset>

                    <h3>Project permissions</h3>
                    <fieldset class="group">
                        <legend><span>All active users can</span></legend>
                        <div class="radio">
                            <input class="radio" type="radio" checked="checked" name="rads" id="irOne">
                            <label for="irOne">Read and Write</label>
                        </div>
                        <div class="radio">
                            <input class="radio" type="radio" name="rads" id="irTwo">
                            <label for="irTwo">Read only</label>
                        </div>
                        <div class="radio">
                            <input class="radio" type="radio" name="rads" id="irThree">
                            <label for="irThree">No Access</label>
                        </div>
                    </fieldset>
                    <div class="buttons-container">
                        <div class="buttons">
                            <input class="aui-button aui-button-primary" type="button" name="submit" value="Create project">
                            <a id="cancel" class="aui-button aui-button-link" name="cancel" accesskey="c" href="#">Cancel</a>
                        </div>
                    </div>
                </form>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div><!-- .aui-page-panel -->

</section>
@endsection