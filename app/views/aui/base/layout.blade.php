@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-group.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Foundation</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <h2>Layout</h2>

                <h3>What problem does this solve?</h3>
                <p>Page layouts offer a range of options for structuring screens in applications. They help organise content and make the user interface understandable.</p>

                <h3>Interactive examples</h3>
                <div class="example-container">
                    <h4>Page layout</h4>
                    <div class="aui-group">
                        <div class="aui-item">
                            <div class="examples-section">
                                <a href="/aui/base/layout/fluid" target="_blank"><div class="example-screenshots example-fluid"></div>Fluid (default)</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/fixed" target="_blank"><div class="example-screenshots example-fixed-width"></div>Fixed width</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/hybrid" target="_blank"><div class="example-screenshots example-hybrid"></div>Hybrid</a>
                            </div>
                        </div>
                    </div>
                    <h4>Content layout</h4>
                    <div class="aui-group">
                        <div class="aui-item">
                            <div class="examples-section">
                                <a href="/aui/base/layout/content" target="_blank"><div class="example-screenshots example-content"></div>Content only</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/nav-content" target="_blank"><div class="example-screenshots example-nav-content"></div>Navigation and content</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/content-sidebar" target="_blank"><div class="example-screenshots example-content-sidebar"></div>Content and sidebar</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/nav-content-sidebar" target="_blank"><div class="example-screenshots example-nav-content-sidebar"></div>Navigation, content and sidebar</a>
                            </div>
                            <div class="examples-section">
                                <a href="/aui/base/layout/focused-task" target="_blank"><div class="example-screenshots example-focused-task"></div>Focused task</a>
                            </div>
                        </div>
                    </div>
                </div>

                <h3>When and how to use this pattern</h3>
                <p>Applications need at least one layout scheme, and may use more than one throughout, depending on the context. This guide aims to outline when the various layouts are most appropriate.</p>
                <p>Layouts are divided into two areas:</p>
                <ul>
                    <li>Page layouts</li>
                    <li>Content layouts</li>
                </ul>

                <h4>Page layouts</h4>
                <p>Applications should only use one page alignment overall, to avoid an inconsistent and jarring experience.</p>

                <div class="aui-group example-typography">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/fluid" target="_blank"><div class="example-screenshots example-inline example-fluid"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Fluid (default layout)</h4>
                        <p>The <em>fluid</em> page layout is the default page layout for Atlassian applications. It provides maximum space for content in all sizes, and leaves control over the size of the UI to the user by automatically adjusting to fit the size of their browser. An issue to be aware of is that some types of content (like large bodies of text) can become hard to read due to excessively long line lengths if nothing is done to mitigate this.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/fixed" target="_blank"><div class="example-screenshots example-inline example-fixed-width"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Fixed width</h4>
                        <p>The <em>fixed</em> width page layout deliberately constrains the overall width of the content and horizontally centers it on the page. This gives the designer more control and can be useful for when a more predictable layout with easy to read line lengths are needed. Conversely, it constrains the amount of space available to content and takes away control over presentation from the user.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/hybrid" target="_blank"><div class="example-screenshots example-inline example-hybrid"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Hybrid</h4>
                        <p>The <em>hybrid</em> page layout combines the fluid design for the application header with a fixed width design for the rest of the content area. It should only be used in cases where a fixed width page layout is desirable but where the <a href="application-header.html">Application header</a> might contain a large number of items.</p>
                        <p>Different structures can be applied to the main content area of a page to suit different purposes.</p>
                    </div>
                </div>


                <h4>Content layouts</h4>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/content" target="_blank"><div class="example-screenshots example-inline example-content"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Content only</h4>
                        <p>The <em>content only</em> layout maximises the space available for content. It provides more horizontal space than the other layouts, and is therefore ideal for displaying wide tables and other large user interface elements. Conversely, it doesn't provide any structure for grouping and separating elements.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/nav-content" target="_blank"><div class="example-screenshots example-inline example-nav-content"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Navigation and content</h4>
                        <p>The <em>navigation and content</em> layout has a column for vertical navigation to the left of the content area, which is useful for when a long list of navigation links is necessary. See <a href="vertical-navigation.html">Vertical navigation</a> and <a href="horizontal-navigation.html">Horizontal navigation</a> for more detail.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/content-sidebar" target="_blank"><div class="example-screenshots example-inline example-content-sidebar"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Content and sidebar</h4>
                        <p>The <em>content and sidebar</em> layout has a column to the right of the content area. The sidebar is used for supplementary content, which would otherwise interrupt the flow of content in the main content area.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/nav-content-sidebar" target="_blank"><div class="example-screenshots example-inline example-nav-content-sidebar"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Navigation, content and sidebar</h4>
                        <p>The <em>navigation, content and sidebar</em> layout is a combination of the three layouts mentioned above. This layout can be used when a <a href="vertical-navigation.html">Vertical navigation</a> is required as well as a two column structure in the content area.</p>
                    </div>
                </div>

                <div class="aui-group">
                    <div class="aui-item example-inline-item">
                        <a href="/aui/base/layout/focused-task" target="_blank"><div class="example-screenshots example-inline example-focused-task"></div></a>
                    </div>
                    <div class="aui-item">
                        <h4>Focused task</h4>
                        <p>The <em>focused task</em> layout has a smaller, fixed width content area that is horizontally centered on the page regardless of which page alignment is applied. See <a href="focused-tasks.html">Focused task</a> page for more details.</p>
                    </div>
                </div>

                <h3>What happens if …</h3>
                <ul>
                    <li><strong>my application needs more than one layout?</strong> There are good reasons to have different content layouts within an application, and this is quite commonly seen in applications. However, the overall page layout of the application should remain consistent.</li>
                    <li><strong>my application has a screen that would benefit from no structured layout at all, for example a wall projection screen?</strong> Certain uses require content to have the maximum available screen space. This, however, should be treated as an exception to the rule, with the understanding that the user flow from one screen to the next is disrupted by such a design.</li>
                </ul>

                <h3>Contextual examples</h3>
                <div class="example-thumbs">
                    <a href="/images/aui/examples/layouts/example-full-01.png" target="_blank"><img src="/images/aui/examples/layouts/example-thumb-01.png"></a>
                    <a href="/images/aui/examples/layouts/example-full-02.png" target="_blank"><img src="/images/aui/examples/layouts/example-thumb-02.png"></a>
                    <a href="/images/aui/examples/layouts/example-full-03.png" target="_blank"><img src="/images/aui/examples/layouts/example-thumb-02.png"></a>
                </div>

                <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
                <ul>
                    <li><a href="http://docs.atlassian.com/aui/5.2/docs/layout.html" target="_blank">AUI Documentation</a> – implementation details</li>
                </ul>

            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>
</section>
@endsection