@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-page-header.css") }}
    {{ HTML::style("/styles/aui/tables.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-group.css") }}
    {{ HTML::style("/styles/aui/aui-lozenge.css") }}
    {{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

<header class="aui-page-header">
    <div class="aui-page-header">
        <div class="aui-page-header-inner">
            <h1>Foundation</h1>
        </div>
    </div>
</header>

<div class="aui-page-panel">
<div class="aui-page-panel-inner">
<section class="aui-page-panel-content">
<h2>Colors</h2>

<p>The color palette is a cornerstone of our Product Design Language, giving our products an immediate sense of belonging to a family, enabling them to be <em>harmonious</em>, one of our five <a href="principles.html">design principles</a>. The color palette aims to leverage Atlassian's brand heritage, and build on its sense of strength, intelligence and confidence, while adding some warmth and a <em>human touch</em> (another of our design principles).</p>
<p>Where appropriate, we enable users to introduce their own color palettes. Our products should adapt intelligently and flexibly to cater for the user's preference.</p>
<p>Atlassian is committed to complying with <a href="http://www.w3.org/TR/WCAG/">AA standard contrast ratios</a> for all text that is not "incidental". The color combinations in this guide comply with the AA standard.</p>

<p><img src="/images/aui/color/chart.png" width="790" height="530" alt="Color chart"></p><p>

</p><h3>Primary palette</h3>
<div class="aui-group">
    <div class="aui-item">
        <div class="colours-section">
            <div class="colours colours-blue"></div>
            <h4>Blue</h4>
            <p>#205081</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-bright-blue-1"></div>
            <h4>Bright blue 1</h4>
            <p>#3b73af</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-charcoal"></div>
            <h4>Charcoal</h4>
            <p>#333333</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-light-grey"></div>
            <h4>Light gray</h4>
            <p>#f5f5f5</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-white"></div>
            <h4>White</h4>
            <p>#ffffff</p>
        </div>
    </div>
</div>

<h3>Secondary palette and derivatives</h3>
<div class="aui-group">
    <div class="aui-item">
        <div class="colours-section">
            <div class="colours colours-bright-blue-2"></div>
            <h4>Bright blue 2</h4>
            <p>#3b7fc4</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-pale-blue"></div>
            <h4>Pale blue</h4>
            <p>#ebf2f9</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-medium-grey"></div>
            <h4>Medium gray</h4>
            <p>#707070</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-green"></div>
            <h4>Green</h4>
            <p>#14892c</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-yellow"></div>
            <h4>Yellow</h4>
            <p>#ffd351</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-brown"></div>
            <h4>Brown</h4>
            <p>#815b3a</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-warm-red"></div>
            <h4>Warm red</h4>
            <p>#d04437</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-blue-grey"></div>
            <h4>Blue-gray</h4>
            <p>#4a6785</p>
        </div>
        <div class="colours-section">
            <div class="colours colours-purple"></div>
            <h4>Purple</h4>
            <p>#654982</p>
        </div>
    </div>
</div>

<div class="aui-group">
    <div class="aui-item">
        <div class="colours-derivative">
            <div class="colours-small colours-medium-blue"></div>
            <p>#ccd9ea</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-grey"></div>
            <p>#999999</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-half-green"></div>
            <p>#89c495</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-half-yellow"></div>
            <p>#ffe9a8</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-brown-2"></div>
            <p>#594300</p>
        </div>
        <div class="colours-derivative">
            &nbsp;
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-half-blue-grey"></div>
            <p>#a4b3c2</p>
        </div>
    </div>
</div>

<div class="aui-group">
    <div class="aui-item">
        <div class="colours-derivative">
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-button-grey"></div>
            <p>#cccccc</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-pale-green"></div>
            <p>#f3f9f4</p>
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-pale-yellow"></div>
            <p>#fffdf6</p>
        </div>
        <div class="colours-derivative">
            &nbsp;
        </div>
        <div class="colours-derivative">
            &nbsp;
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-pale-blue-grey"></div>
            <p>#f6f7f9</p>
        </div>
    </div>
</div>

<div class="aui-group">
    <div class="aui-item">
        <div class="colours-derivative">
        </div>
        <div class="colours-derivative">
            <div class="colours-small colours-highlight-grey"></div>
            <p>#e9e9e9</p>
        </div>
    </div>
</div>

<h3>Usage</h3>
<h4>Primary palette</h4>
<p>The primary palette includes colors used in the main interface chrome, interaction elements and states. Shades, tints and other derivatives of these colors are not to be used.</p>
<table class="aui">
    <thead>
    <tr>
        <th id="basic-name" style="width:150px">Name</th>
        <th id="basic-colour" style="width:100px">Colour</th>
        <th id="basic-hex" style="width:100px">HEX Value</th>
        <th id="basic-description">Main usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-name">Blue</td>
        <td headers="basic-colour">
            <div class="colours-small colours-blue"></div>
        </td>
        <td headers="basic-hex">#205081</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="application-header.html">Application header background</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Bright blue 1</td>
        <td headers="basic-colour">
            <div class="colours-small colours-bright-blue-1"></div>
        </td>
        <td headers="basic-hex">#3b73af</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="typography.html">Hyperlink</a></li>
                <li><a href="application-header.html">Dropdown menu item hover background</a></li>
                <li><a href="application-header.html">App switcher hover</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Charcoal</td>
        <td headers="basic-colour">
            <div class="colours-small colours-charcoal"></div>
        </td>
        <td headers="basic-hex">#333333</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="typography.html">Standard body text</a></li>
                <li>Default foreground colour</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Light gray</td>
        <td headers="basic-colour">
            <div class="colours-small colours-light-grey"></div>
        </td>
        <td headers="basic-hex">#f5f5f5</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="page-header.html">Page header background</a></li>
                <li><a href="toolbar.html">Toolbar background</a> (optional)</li>
                <li>Panel background (optional)</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">White</td>
        <td headers="basic-colour">
            <div class="colours-small colours-white"></div>
        </td>
        <td headers="basic-hex">#ffffff</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Page background</li>
                <li><a href="application-header.html">Dropdown menu item foreground hover</a></li>
                <li><a href="lozenges.html">Lozenge (standard) foreground</a></li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<h4>Secondary palette</h4>
<p>The secondary palette includes colors used in content areas and some interaction elements and states, as well as highlight colors used for communicators such as icons and status lozenges. Derivatives of these colors may be used where appropriate. See further below for guidance for creating derivative colors.</p>
<table class="aui">
    <thead>
    <tr>
        <th id="basic-name" style="width:150px">Name</th>
        <th id="basic-colour" style="width:100px">Color</th>
        <th id="basic-hex" style="width:100px">HEX Value</th>
        <th id="basic-description">Main usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-name">Bright blue 2</td>
        <td headers="basic-colour">
            <div class="colours-small colours-bright-blue-2"></div>
        </td>
        <td headers="basic-hex">#3b7fc4</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="application-header.html">Create button</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Pale blue</td>
        <td headers="basic-colour">
            <div class="colours-small colours-pale-blue"></div>
        </td>
        <td headers="basic-hex">#ebf2f9</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Selection background</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Medium Gray</td>
        <td headers="basic-colour">
            <div class="colours-small colours-medium-grey"></div>
        </td>
        <td headers="basic-hex">#707070</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="forms.html">Form label foreground</a></li>
                <li><a href="typography.html">Form helper foreground</a></li>
                <li><a href="typography.html">Placeholder foreground</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Green</td>
        <td headers="basic-colour">
            <div class="colours-small colours-green"></div>
        </td>
        <td headers="basic-hex">#14892c</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="lozenges.html">Success lozenge</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Yellow</td>
        <td headers="basic-colour">
            <div class="colours-small colours-yellow"></div>
        </td>
        <td headers="basic-hex">#ffd351</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="lozenges.html">Current lozenge</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Brown</td>
        <td headers="basic-colour">
            <div class="colours-small colours-brown"></div>
        </td>
        <td headers="basic-hex">#815b3a</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="lozenges.html">Moved lozenge</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Warm red</td>
        <td headers="basic-colour">
            <div class="colours-small colours-warm-red"></div>
        </td>
        <td headers="basic-hex">#d04437</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="lozenges.html">Error lozenge</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Blue-gray</td>
        <td headers="basic-colour">
            <div class="colours-small colours-blue-grey"></div>
        </td>
        <td headers="basic-hex">#4a6785</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="lozenges.html">Complete lozenge</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Purple</td>
        <td headers="basic-colour">
            <div class="colours-small colours-purple"></div>
        </td>
        <td headers="basic-hex">#654982</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Visited hyperlink foreground</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<h4>Don't</h4>
<ul>
    <li>Modify colors in the primary palette</li>
    <li>Use foreground and background color combinations that are not <a href="http://www.w3.org/TR/WCAG/">WCAG 2.0 AA-level</a> compliant</li>
    <li>Rely on color alone to convey state or meaning</li>
</ul>

<h4>Derivative colors</h4>
<p>Color variations are derived from the secondary palette, for example, when solid background colors are required. Ways to create derivatives include lowering the saturation, brightness or alpha values of the secondary color palette.</p>

<table class="aui">
    <thead>
    <tr>
        <th id="basic-name" style="width:150px">Name</th>
        <th id="basic-colour" style="width:100px">Color</th>
        <th id="basic-hex" style="width:100px">HEX Value</th>
        <th id="basic-description">Main usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td headers="basic-name">Pale blue derivative</td>
        <td headers="basic-colour">
            <div class="colours-small colours-medium-blue"></div>
        </td>
        <td headers="basic-hex">#ccd9ea</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>The border of selected items that use the pale blue background color #ebf2f9</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Gray derivative</td>
        <td headers="basic-colour">
            <div class="colours-small colours-grey"></div>
        </td>
        <td headers="basic-hex">#999999</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li><a href="forms.html">Form placeholder text</a></li>
                <li><a href="buttons.html">Disabled button text</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Gray derivative</td>
        <td headers="basic-colour">
            <div class="colours-small colours-border-grey"></div>
        </td>
        <td headers="basic-hex">#cccccc</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Default border and outline color</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Medium green</td>
        <td headers="basic-colour">
            <div class="colours-small colours-half-green"></div>
        </td>
        <td headers="basic-hex">#89c495</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Border for <a href="messages.html">success messages</a> and <a href="lozenges.html">subtle lozenges</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Pale green</td>
        <td headers="basic-colour">
            <div class="colours-small colours-pale-green"></div>
        </td>
        <td headers="basic-hex">#f3f9f4</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Background for <a href="messages.html">success messages</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Medium yellow</td>
        <td headers="basic-colour">
            <div class="colours-small colours-half-yellow"></div>
        </td>
        <td headers="basic-hex">#ffe9a8</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Border for <a href="messages.html">warning messages</a> and <a href="lozenges.html">subtle lozenges</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Pale yellow</td>
        <td headers="basic-colour">
            <div class="colours-small colours-pale-yellow"></div>
        </td>
        <td headers="basic-hex">#fffdf6</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Background for <a href="messages.html">warning messages</a> and <a href="lozenges.html">subtle lozenges</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Dark brown</td>
        <td headers="basic-colour">
            <div class="colours-small colours-brown-2"></div>
        </td>
        <td headers="basic-hex">#594300</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Text for <a href="lozenges.html">current lozenges</a> to maintain AA contrast ratios</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Medium blue-gray</td>
        <td headers="basic-colour">
            <div class="colours-small colours-half-blue-grey"></div>
        </td>
        <td headers="basic-hex">#a4b3c2</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Border for <a href="messages.html">info messages</a> and <a href="lozenges.html">subtle lozenges</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td headers="basic-name">Pale blue-gray</td>
        <td headers="basic-colour">
            <div class="colours-small colours-pale-blue-grey"></div>
        </td>
        <td headers="basic-hex">#f6f7f9</td>
        <td headers="basic-description">
            <ul class="no-bullets">
                <li>Background for <a href="messages.html">info messages</a> and <a href="lozenges.html">subtle lozenges</a></li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>

<h3>Subtle hovers</h3>
<p>When there is the need for a more subtle hover treatment than the main treatment in dropdown menus <span class="aui-lozenge aui-lozenge-code">#3b73af</span>, the secondary ("subtle") hover can be used. This uses <span class="aui-lozenge aui-lozenge-code">rgba (0,0,0,0.11)</span> color to achieve a mostly transparent background color that is visually identical to light gray <span class="aui-lozenge aui-lozenge-code">#f5f5f5</span> when used on a white background.</p>
<p>This subtle hover can be used on tabs, table rows, vertical navigation and horizontal navigation. Use the fallback of light gray <span class="aui-lozenge aui-lozenge-code">#f5f5f5</span> for browsers that don't support RGBA color.</p>

<h3>Rationale</h3>
<ul>
    <li><strong>Brand heritage</strong>
        <p>An evolution building on what came before, keeping the familiar blue, white and gray.</p>
    </li>
    <li><strong>Strength, intelligence, confidence</strong>
        <p>The primary color palette emphasises Atlassian's strong position in the market with understated confidence.</p>
    </li>
    <li><strong>A touch of warmth</strong>
        <p>Judicious use of vibrant colors adds a human touch.</p>
    </li>
    <li><strong>Accessibility</strong>
        <p>The color contrast recommendations here reflect Atlassian's inclusive design approach and position as thought leaders. Additionally, they give Atlassian's products the broadest possible reach, catering for customers who are bound by accessibility and anti-discriminatory regulation.</p>
    </li>
</ul>

</section><!-- .aui-page-panel-content -->
</div><!-- .aui-page-panel-inner -->
</div>
</section>
@endsection