@section("styles")@parent
    {{ HTML::style("/styles/aui/aui-header.css") }}
    {{ HTML::style("/styles/aui/overrides/aui-header.css") }}
    {{ HTML::style("/styles/aui/dropdown2.css") }}
    {{ HTML::style("/styles/aui/aui-navigation.css") }}
    {{ HTML::style("/styles/aui/aui-avatars.css") }}
    {{ HTML::style("/styles/aui/aui-buttons.css") }}
    {{ HTML::style("/styles/aui/icons.css") }}
    {{ HTML::style("/styles/aui/aui-iconfont.css") }}
@endsection

@section('javascript-cdata')@parent
require(['libs/ajs/aui-header-responsive']);
@endsection

@section("header")
<header id="header" role="banner">
    <nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
        <div class="aui-header-inner">
            <div class="aui-header-before">
                <a class="aui-dropdown2-trigger app-switcher-trigger" aria-owns="app-switcher" id="app-switcher-trigger">
                    <span class="aui-icon aui-icon-small aui-iconfont-appswitcher">Design/AUI</span>
                </a>
                <div class="aui-dropdown2 aui-style-default" id="app-switcher">
                    <ul>
                        <li><a class="aui-dropdown2-radio interactive checked">AUI Exemplos</a></li>
                    </ul>
                </div>
            </div>
            <div class="aui-header-primary">
                <h1 id="logo" class="aui-header-logo aui-header-logo-aui">
                    <a href="/aui">
                        <span class="aui-header-logo-device">AUI</span>
                    </a>
                </h1>
                <ul class="aui-nav">
                    <li>
                        <a href="#" aria-owns="base-dropdown" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="base-dropdown">
                            Foundation<span class="aui-icon-dropdown"></span>
                        </a>
                        <div id="base-dropdown" class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" data-dropdown2-alignment="left" aria-hidden="false">
                            <ul>
                                <li>
                                    <a href="/aui/base/grid" class="">Grid</a>
                                </li>
                                <li>
                                    <a href="/aui/base/layout">Layout</a>
                                </li>
                                <li>
                                    <a href="/aui/base/typography">Typography</a>
                                </li>
                                <li>
                                    <a href="/aui/base/colors">Colors</a>
                                </li>
                                <li>
                                    <a href="/aui/base/iconography">Iconography</a>
                                </li>
                                <li>
                                    <a href="/aui/base/avatars" class="">Avatars</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#" aria-owns="controls-dropdown" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="controls-dropdown">
                            Controls<span class="aui-icon-dropdown"></span>
                        </a>
                        <div id="controls-dropdown" class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" data-dropdown2-alignment="left" aria-hidden="false">
                            <ul>
                                <li>
                                    <a href="/aui/controls/buttons">Buttons</a>
                                </li>
                                <li>
                                    <a href="/aui/controls/forms">Forms</a>
                                </li>
                                <li>
                                    <a href="/aui/controls/tables">Tables</a>
                                </li>
                                <li>
                                    <a href="/aui/controls/tabs">Tabs</a>
                                </li>
                                <li>
                                    <a href="/aui/controls/toolbar">Toolbar</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="aui-header-secondary">
                <ul class="aui-nav">
                    <li>
                        <a href="#" aria-owns="help-dropdown" aria-haspopup="true" class="aui-dropdown2-trigger" aria-controls="dropdown2-help">
                            <span class="aui-icon aui-icon-small aui-iconfont-help">Ajuda</span>
                        </a>
                        <div id="help-dropdown" class="aui-dropdown2 aui-style-default aui-dropdown2-in-header" data-dropdown2-alignment="right" aria-hidden="false">
                            <ul>
                                <li><a href="https://developer.atlassian.com/design/latest/index.html" class="active" target="_blank">Design Guidelines</a></li>
                                <li><a href="https://docs.atlassian.com/aui/latest/docs/index.html" target="_blank">AUI Documentation</a></li>
                                <li><a href="https://docs.atlassian.com/aui/latest/sandbox/" target="_blank">AUI Sandbox</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
@endsection