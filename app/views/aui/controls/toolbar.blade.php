@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/aui/aui-toolbar2.css") }}
{{ HTML::style("/styles/aui/overrides/aui-group.css") }}
{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Controls</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <h2>Toolbar</h2>

                <p>The toolbar is a group of buttons that give users access to frequently performed actions of a focused task. Common toolbar placements can be found on a JIRA issue or while editing a Confluence page.</p>

                <h3>Interactive example</h3>
                <div class="example-container">
                    <div class="aui-toolbar2">
                        <div class="aui-toolbar-2-inner">
                            <div class="aui-toolbar2-primary">
                                <div id="button-set" class="aui-buttons">
                                    <button class="aui-button aui-button-primary">Edit</button>
                                </div>
                                <div id="button-set" class="aui-buttons">
                                    <button class="aui-button">Assign</button>
                                    <button class="aui-button">Assign to me</button>
                                    <button class="aui-button">Comment</button>
                                    <button class="aui-button aui-dropdown2-trigger" href="#dropdown2-more" aria-owns="dropdown2-more" aria-haspopup="true" aria-controls="dropdown2-more">More</button>
                                </div>
                                <div id="button-set" class="aui-buttons">
                                    <button class="aui-button">Reopen</button>
                                    <button class="aui-button">Statistics</button>
                                </div>
                            </div>
                            <div class="aui-toolbar2-secondary">
                                <div id="button-set" class="aui-buttons">
                                    <button class="aui-button aui-dropdown2-trigger" href="#dropdown2-view" aria-owns="dropdown2-view" aria-haspopup="true" aria-controls="dropdown2-view"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                                </div>
                            </div>
                        </div><!-- .aui-toolbar-inner -->
                    </div><!-- .aui-toolbar2 -->
                </div><!-- .example-container -->





                <div id="dropdown2-more" class="aui-dropdown2 aui-style-default" style="display: none; top: 356px; left: 351px;" aria-hidden="true" data-dropdown2-alignment="left">
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Agile board</a></li>
                            <li><a href="#" class="">Rank to top</a></li>
                            <li><a href="#" class="">Rank to bottom</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Log work</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Attach files</a></li>
                            <li><a href="#" class="">Attach screenshot</a></li>
                            <li><a href="#" class="">Add/Edit UI mockup</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Voters</a></li>
                            <li><a href="#" class="">Watch issue</a></li>
                            <li><a href="#" class="">Watchers</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Create sub-task</a></li>
                            <li><a href="#" class="">Convert to sub-task</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="">Move</a></li>
                            <li><a href="#" class="">Link</a></li>
                            <li><a href="#" class="">Clone</a></li>
                            <li><a href="#" class="">Labels</a></li>
                            <li><a href="#">Create test session</a></li>
                        </ul>
                    </div>
                    <div class="aui-dropdown2-section">
                        <ul class="aui-list-truncate">
                            <li><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div><div id="dropdown2-view" class="aui-dropdown2 aui-style-default" style="display: none; top: 356px; left: 1704px;" aria-hidden="true" data-dropdown2-alignment="right">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="">XML</a></li>
                        <li><a href="#" class="">Word</a></li>
                        <li><a href="#" class="">Print version</a></li>
                    </ul>
                </div><h3>Usage</h3>
                <ul>
                    <li>Create toolbar buttons for the actions users need</li>
                    <li>Order the buttons in a toolbar for actions that are used regularly or are significant</li>
                    <li>Buttons should be grouped into sets within the toolbar and ordered based on usage and importance</li>
                    <li>Ensure that the action of a <a href="buttons.html">button</a> is clearly communicated to the user</li>
                    <li>The main action of a toolbar set can be a toolbar primary button</li>
                </ul>

                <h3>Contextual examples</h3>
                <div class="example-thumbs">
                    <a href="/images/aui/examples/toolbar/example-full-01.png" target="_blank"><img src="/images/aui/examples/toolbar/example-thumb-01.png" alt=""></a>
                    <a href="/images/aui/examples/toolbar/example-full-02.png" target="_blank"><img src="/images/aui/examples/toolbar/example-thumb-02.png" alt=""></a>
                    <a href="/images/aui/examples/toolbar/example-full-03.png" target="_blank"><img src="/images/aui/examples/toolbar/example-thumb-03.png" alt=""></a>
                </div>

                <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
                <ul>
                    <li><a href="http://docs.atlassian.com/aui/5.2/docs/toolbar2.html" target="_blank">AUI Documentation</a> – implementation details</li>
                    <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=toolbar" target="_blank">AUI Sandbox</a> – code snippets</li>
                </ul>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection