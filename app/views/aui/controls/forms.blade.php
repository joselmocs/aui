@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/aui/tables.css") }}
{{ HTML::style("/styles/aui/forms.css") }}
{{ HTML::style("/styles/aui/aui-lozenge.css") }}
{{ HTML::style("/styles/aui/overrides/aui-group.css") }}
{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Controls</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Forms</h2>

    <p>Forms are used to collect user input and configure options of a task the user is completing.</p>

    <h3>Interactive example</h3>
    <div class="example-container">
        <form action="#" method="post" id="d" class="aui">
            <fieldset>
                <div class="field-group">
                    <label for="d-fname">Default field<span class="aui-icon icon-required"> required</span></label>
                    <input class="text" type="text" id="d-fname" name="d-fname" title="first name">
                    <div class="description">Default width input of a required field</div>
                </div>
                <div class="field-group">
                    <label for="d-lname">Long field</label>
                    <input class="text long-field" type="text" id="d-lname" name="d-lname" title="last name">
                    <div class="error">Error message here</div>
                    <div class="description">Long width input</div>
                </div>
                <div class="field-group">
                    <label for="d-fname">Short field</label>
                    <input class="text short-field" type="text" id="d-fname" name="d-fname" title="first name">
                    <div class="description">Short width input</div>
                </div>
                <div class="field-group">
                    <label for="d-fname">Disabled field</label>
                    <input class="text" type="text" id="d-fname" name="d-fname" title="first name" disabled="">
                    <div class="description">Disable field input</div>
                </div>
                <div class="field-group">
                    <label for="email1">Email</label>
                    <input class="text medium-field" type="text" id="email1" name="email" title="email" placeholder="you@example.com">
                    <span class="aui-icon icon-help">help</span>
                    <div class="description">Medium width input</div>
                </div>
                <div class="field-group">
                    <label for="password1" accesskey="p">Password</label>
                    <input class="password" type="password" id="password1" name="password" title="password">
                </div>
            </fieldset>

            <h3>Dropdowns and multi select</h3>
            <fieldset>
                <legend><span>Dropdowns and multi select</span></legend>
                <div class="field-group">
                    <label for="dBase">Dropdown</label>
                    <select class="select" id="dBase" name="dBase" title="database select">
                        <option>Select</option>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <optgroup label="Group 1">
                            <option>Option one</option>
                            <option>Option two</option>
                        </optgroup>
                        <option>Option 3</option>
                        <option>Option 4</option>
                    </select>
                </div>
                <div class="field-group">
                    <label for="multiselect">Multi select</label>
                    <select class="multi-select" size="4" multiple="multiple" id="multiselect" name="multiselect">
                        <option>option one</option>
                        <option>option two</option>
                        <option>option three</option>
                        <option>option four</option>
                        <option>option five</option>
                        <option>option six</option>
                    </select>
                    <div class="description">Multi select description</div>
                </div>
            </fieldset>

            <h3>Textarea</h3>
            <fieldset>
                <legend><span>Textarea Legend</span></legend>
                <div class="field-group">
                    <label for="comment">Comment</label>
                    <textarea class="textarea" name="comment" id="comment" placeholder="Your comment here…"></textarea>
                </div>
                <div class="field-group">
                    <label for="licenseKey">License key</label>
                    <textarea class="textarea" rows="6" cols="10" name="licenseKey" id="licenseKey"></textarea>
                </div>
            </fieldset>
            <div class="buttons-container">
                <div class="buttons">
                    <input class="button submit" type="submit" value="Save" id="d-save-btn1">
                    <a class="cancel" href="#">Cancel</a>
                </div>
            </div>

            <h3>Radio buttons</h3>
            <fieldset class="group">
                <legend><span>Radio buttons</span></legend>
                <div class="radio">
                    <input class="radio" type="radio" checked="checked" name="rads" id="irOne">
                    <label for="irOne">Save as a blog post</label>
                </div>
                <div class="radio">
                    <input class="radio" type="radio" name="rads" id="irTwo">
                    <label for="irTwo">Save as a page</label>
                </div>
                <div class="radio">
                    <input class="radio" type="radio" name="rads" id="irThree">
                    <label for="irThree">Save to your drafts</label>
                </div>
            </fieldset>

            <h3>Checkboxes</h3>
            <fieldset class="group">
                <legend><span>Checkboxes</span></legend>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbOne" id="cbOne">
                    <label for="cbOne">Receive email</label>
                </div>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbTwo" id="cbTwo">
                    <label for="cbTwo">Receive push notification</label>
                </div>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbThree" id="cbThree">
                    <label for="cbThree">Receive in-app notification</label>
                </div>
            </fieldset>

            <h3>File upload</h3>
            <fieldset>
                <legend><span>File upload<span class="aui-icon icon-required"></span></span></legend>
                <div class="field-group">
                    <label for="uploadFile">Upload file</label>
                    <input class="upfile" type="file" id="uploadFile" name="uploadFile" title="upload file">
                </div>
            </fieldset>
        </form>
    </div>

    <h3>General usage</h3>

    <h4>Do</h4>
    <ul>
        <li>Use right-aligned labels before considering top-aligned labels</li>
        <li>Use an <span class="aui-lozenge aui-lozenge-code">&lt;h3&gt;</span> for a sub-heading inside of a form</li>
        <li>Use short descriptions for help text on a single line. If the description is long, disclose behind the (?) icon for inline help expand</li>
        <li>Use sentence case for labels in accordance with our <a href="language.html">writing formats</a> guideline</li>
    </ul>

    <h4>Don't</h4>
    <ul>
        <li>Use left-aligned labels</li>
        <li>Truncate field labels - long labels should wrap</li>
        <li>Have the full stop at the end of description text. If more than a single sentence is required use a full on the first sentence or consider using the (?) icon for longer descriptions</li>
    </ul>

    <h3>Mandatory fields</h3>
    <p>Mandatory and optional fields should be marked according to "the rule of least noise".</p>
    <ul>
        <li>If the majority of your fields are mandatory, mark optional fields with "(optional)"</li>
        <li>If the majority of fields are optional, mark mandatory fields with the <span style="color:#d04437;">*</span></li>
        <li>If all your fields are either mandatory or optional, don't mark any</li>
    </ul>

    <h3>Field length</h3>
    <p>Length of input fields should communicate the intended content.</p>
    <ul>
        <li>Fields don't need to all be the same length. Mix long and short field length according to the content it is used for</li>
        <li>AUI provides short, medium, long and full width options for fields</li>
        <li>You can override input field widths if you have specific character length requirements for the field (eg. postcode)</li>
    </ul>

    <h3>Radio buttons</h3>
    <p>Radio buttons are often used incorrectly as a form control. The radio button group provides a set of options that are mutually exclusive.</p>

    <div class="example-container">
        <form class="aui">
            <fieldset class="group">
                <legend><span>Radio buttons</span></legend>
                <div class="radio">
                    <input class="radio" type="radio" checked="checked" name="rads" id="irOne">
                    <label for="irOne">Save as a blog post</label>
                </div>
                <div class="radio">
                    <input class="radio" type="radio" name="rads" id="irTwo">
                    <label for="irTwo">Save as a page</label>
                </div>
                <div class="radio">
                    <input class="radio" type="radio" name="rads" id="irThree">
                    <label for="irThree">Save to your drafts</label>
                </div>
            </fieldset>
        </form>
    </div>

    <h4>Do</h4>
    <ul>
        <li>Use strong labels on radio buttons that clearly notify the user what the option means</li>
        <li>Use sentence case for labels</li>
    </ul>

    <h4>Don't</h4>
    <ul>
        <li>Don't use radio buttons to display a set of choices to a user. Instead you should use checkboxes in cases where more than one of those options can be selected</li>
        <li>Don't use radio buttons as actions, instead use the <a href="buttons.html">buttons guide</a> for actions</li>
    </ul>

    <h3>Checkboxes</h3>
    <p>Checkboxes are used when there are multiple choices available for a single group of options. They are used to describe the action or state of an item that can be turned on or off.</p>
    <div class="example-container">
        <form class="aui">
            <fieldset class="group">
                <legend><span>Checkboxes</span></legend>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbOne" id="cbOne">
                    <label for="cbOne">Receive email</label>
                </div>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbTwo" id="cbTwo">
                    <label for="cbTwo">Receive push notification</label>
                </div>
                <div class="checkbox">
                    <input class="checkbox" type="checkbox" name="cbThree" id="cbThree">
                    <label for="cbThree">Receive in-app notification</label>
                </div>
            </fieldset>
        </form>
    </div>

    <h4>Do</h4>
    <ul>
        <li>Use labels that clearly inform the user that there are 2 states available</li>
        <li>Use sentence case for checkbox labels</li>
        <li>Align checkboxes vertically where possible to show that the checkboxes are part of a group</li>
    </ul>

    <h4>Don't</h4>
    <ul>
        <li>Use checkboxes for mutually exclusive choices of a group. Instead use radio buttons</li>
        <li>Use checkboxes to perform an action</li>
    </ul>

    <h3>Contextual examples</h3>
    <div class="example-thumbs">
        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="images/forms/example-full-01.png" title=""><img src="images/forms/example-thumb-01.png" alt=""></a>
        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="images/forms/example-full-02.png" title=""><img src="images/forms/example-thumb-02.png" alt=""></a>
        <a class="fancybox-thumbs" data-fancybox-group="thumb" href="images/forms/example-full-03.png" title=""><img src="images/forms/example-thumb-03.png" alt=""></a>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/forms.html" target="_blank">AUI Documentation</a> – implementation details</li>
        <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=forms" target="_blank">AUI Sandbox</a> – code snippets</li>
    </ul>
    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection