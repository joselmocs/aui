@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/aui/tables.css") }}
{{ HTML::style("/styles/aui/aui-tables-sortable.css") }}

{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("javascript-cdata")@parent
require(["libs/ajs/aui-tables-sortable"]);
@endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Controls</h1>
            </div>
        </div>
    </header>
    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Tables</h2>

    <h3>What problem does this solve?</h3>
    <p>Tables are used to make large volumes of data easy to understand and to access.</p>

    <h3>When and how to use this pattern?</h3>
    <p>The use of tables can range from very simple use cases to extremely complex ones, and will need to be decided on a case-by-case basis. Here are some guidelines to consider when designing data in a table:</p>
    <ul>
        <li>Make sure a table is the most appropriate format for your data to be displayed &ndash; the main task of your table is to reveal data to your users so they can make decisions based on it. <li>Sometimes, graphics, lists or other user interface elements can do a better job than a table when it comes to revealing patterns.</li>
        <li>Understand the context your table is viewed in and design it accordingly &ndash; make sure you know the use cases in which your users interact with the table, then carefully pick the data you show and the table functionality you provide to support these use cases</li>
        <li>Balance speed of delivery with value of data &ndash; your users won't need all available data at the same time, especially not when providing the data can take a significant amount of time. </li>
        <li>Let your users adjust the data &ndash; particularly in high-usage tables, your users will appreciate it if they can adjust the data in it, like resorting it, adding to or subtracting from it, etc. Your initial design should cover the most common use cases, but make sure you cater for some of the less frequent use cases via these customization options.</li>
    </ul>

    <h3>Basic table components</h3>
    <p>As table design needs to take into account the main use cases of a page, designers can make use of various table components and combine them in a sensible manner.</p>

    <h4>I. Table headings</h4>
    <p>By default, tables should have table headings to make it easier for users. If the nature of the content for each column is obvious, it can be decided to not display these headings. If in doubt, use table headings.</p>

    <h4>II. Content alignment</h4>
    <p>Left align cell content except where alternate alignment will aid comprehension. For example, numeric data may be easier to understand when right aligned. Column headers should align to their content.</p>

    <h4>Interactive example &ndash; basic table</h4>
    <div class="example-container narrow">
        <table class="aui aui-table-interactive">
            <thead>
            <tr>
                <th id="basic-number">#</th>
                <th id="basic-fname">First name</th>
                <th id="basic-lname">Last name</th>
                <th id="basic-username">Username</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td headers="basic-number">1</td>
                <td headers="basic-fname">Matt</td>
                <td headers="basic-lname">Bond</td>
                <td headers="basic-username">mbond</td>
            </tr>
            <tr>
                <td headers="basic-number">2</td>
                <td headers="basic-fname">Ross</td>
                <td headers="basic-lname">Chaldecott</td>
                <td headers="basic-username">rchaldecott</td>
            </tr>
            <tr>
                <td headers="basic-number">3</td>
                <td headers="basic-fname">Henry</td>
                <td headers="basic-lname">Tapia</td>
                <td headers="basic-username">htapia</td>
            </tr>
            </tbody>
        </table>
    </div>

    <h4>III. Content width in table cells</h4>
    <p>Ensure a minimum width of columns so content is displayed within a reasonable amount of space.</p>
    <p>By default, long text in a table cell will wrap. However, you may trunctate long text with ellipses if there is another way to access the full text (e.g. on a subsequent page, expanding the cell, etc.). If you have more than one cell with long content, decide to either wrap both or truncate both &ndash; do not mix the two techniques.</p>

    <h4>IV. Content links</h4>
    <p>A given table row may expose one or more links as part of the row content. These should be links off to other locations, not actions. If a content link leads to an external page outside of the application, open it in a new browser tab.</p>

    <h4>V. Disabled or out-of-date rows</h4>
    <p>Rows may be disabled if they are no longer editable by the user, or if their content is no longer as important as other content in the table. Make the following changes to a disabled row:</p>
    <ul>
        <li>Show all elements of the row in a disabled visual state ("grayed out")</li>
        <li>Disable inline editing</li>
        <li>Inline actions and links that are still relevant and functional, should still function</li>
        <li>If the reason for disabling the content is not apparent to the user, provide context</li>
    </ul>

    <h4>VI. Column sorting</h4>
    <p>By default, sorting of table columns is switched off. When enabled, users can click on one of the table column headings to sort the column in ascending order. Clicking it again sorts the same column in descending order. Clicking on another table column heading will sort the table again &ndash; combined sorting with several columns is not supported.</p>
    <p>If sorting is considered beneficial to the user, please consider the following implications enabling it:</p>
    <ul>
        <li>Sorting can be resource intense, and can lead to long wait times</li>
        <li>Tables that scroll infinitely cannot be sorted</li>
    </ul>

    <h4>Interactive example &ndash; column sorting</h4>
    <div class="example-container narrow">
        <table class="aui aui-table-interactive aui-table-sortable">
            <thead>
            <tr>
                <th>Author</th>
                <th>Title</th>
                <th class="aui-table-column-unsortable">Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="#">Joel Unger</a></td>
                <td>DT-101 Update the new logo with some improved styles</td>
                <td class="adg-actions">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Ross Chaldecott</a></td>
                <td>DT-120 Create new styles for pages</td>
                <td class="adg-actions">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Jerry Gordinier</a></td>
                <td>DT-124 Added panels to the right most page containers</td>
                <td class="adg-actions">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


    <h3>Inline actions</h3>

    <h4>Overview</h4>
    <p>An action that can be performed on an individual table row is called an inline action.</p>
    <ul>
        <li>A given table row may have one or more inline actions</li>
        <li>Inline actions always go in the far right column, which has the heading 'Actions' (if headings are shown)</li>
        <li>Inline actions should use an icon button (see Buttons) if a corresponding icon exists. If no icon exists use a link button. Standard buttons and subtle buttons should not be used for inline actions</li>
        <li>As a default, all inline actions should be visible at all times. Only use on-hover visibility of inline actions if the continuous display of them could lead to confusion, or if the resulting table creates too much visual noise</li>
        <li>If an inline action is a toggle (e.g. watch/unwatch) then all actions must be visible</li>
        <li>A maximum of three inline actions can be visible at the same time. Once a fourth (or more) action is offered, leave the two main actions visible and collapse the other two or more in an 'cog' menu ('Configure' under <a href="iconography.html">Iconography</a>), which is a <a href="buttons.html">compact button</a>. It is also possible to just show one inline action, with all the others collapsed in the cog menu</li>
    </ul>

    <h4>Interactive example &ndash; all inline actions visible</h4>
    <div class="example-container narrow">
        <table id="adg-table-1" class="aui aui-table-interactive">
            <thead>
            <tr>
                <th>Author</th>
                <th>Title</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="#">Joel Unger</a></td>
                <td>DT-101 Update the new logo with some improved styles</td>
                <td class="aui-compact-button-column">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Ross Chaldecott</a></td>
                <td>DT-120 Create new styles for pages</td>
                <td class="aui-compact-button-column">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Jerry Gordinier</a></td>
                <td>DT-124 Added panels to the right most page containers</td>
                <td class="aui-compact-button-column">
                    <a href="#">Assign</a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                            <li><a href="#" class="">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <h4>Interactive example &ndash; inline actions on hover</h4>
    <div class="example-container narrow">
        <table class="aui aui-table-interactive" id="adg-table-2">
            <thead>
            <tr>
                <th>Project</th>
                <th>Plan</th>
                <th>Build</th>
                <th>Result</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="#">Angry Birds</a></td>
                <td>Normal flight mode</td>
                <td>Takeoff from launch facility</td>
                <td>Crash landing</td>
                <td class="adg-on-hover adg-on-hover-100 aui-compact-button-column">
                    <div class="inline-actions">
                        <button aria-owns="dropdown-button2" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-2"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                        <div id="dropdown-button2" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                            <ul class="aui-list-truncate">
                                <li><a href="#" class="active">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Angry Birds</a></td>
                <td>In-air piloting</td>
                <td>Loop the loop</td>
                <td>Dizziness</td>
                <td class="adg-on-hover adg-on-hover-100 aui-compact-button-column">
                    <div class="inline-actions">
                        <button aria-owns="dropdown-button2" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-2"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                        <div id="dropdown-button2" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                            <ul class="aui-list-truncate">
                                <li><a href="#" class="active">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Angry Birds</a></td>
                <td>Normal flight mode</td>
                <td>Landing on runway</td>
                <td>Runway not found</td>
                <td class="adg-on-hover adg-on-hover-100 aui-compact-button-column">
                    <div class="inline-actions">
                        <button aria-owns="dropdown-button2" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-2"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                        <div id="dropdown-button2" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                            <ul class="aui-list-truncate">
                                <li><a href="#" class="active">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                                <li><a href="#" class="">Action</a></li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <h4>Icons that are not actions</h4>
    <p>A static icon may be used to highlight a particular piece of content. Such icons are not to be used as actions and should not link to another page. Icon buttons must appear in the right most 'Actions' column.</p>

    <h4>Interactive example &ndash; icons that are not actions</h4>
    <div class="example-container narrow">
        <table class="aui aui-table-interactive">
            <thead>
            <tr>
                <th>Author</th>
                <th>Title</th>
                <th>Comments</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><a href="#">Joel Unger</a></td>
                <td>DT-101 Update the new logo with some improved styles</td>
                <td><div><span class="aui-icon aui-icon-small aui-iconfont-comment">Comment</span> <small>7</small></div></td>
                <td class="aui-compact-button-column">
                    <a href="#"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit</span></a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Ross Chaldecott</a></td>
                <td>DT-120 Create new styles for pages</td>
                <td><div><span class="aui-icon aui-icon-small aui-iconfont-comment"></span> <small>8</small></div></td>
                <td class="aui-compact-button-column">
                    <a href="#"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit</span></a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td><a href="#">Jerry Gordinier</a></td>
                <td>DT-124 Added panels to the right most page containers</td>
                <td><div><span class="aui-icon aui-icon-small aui-iconfont-comment"></span> <small>7</small></div></td>
                <td class="aui-compact-button-column">
                    <a href="#"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit</span></a>
                    <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger" data-container="#adg-table-1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                    <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" aria-hidden="false" data-dropdown2-alignment="right">
                        <ul class="aui-list-truncate">
                            <li><a href="#" class="active">Action</a></li>
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Action</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <h3>Editable tables</h3>
    <p>A table is editable if a whole row or data within a row can be added, edited or removed inline, i.e. without leaving the page.</p>

    <h4>States</h4>
    <ul>
        <li>When a user adds, edits or deletes a row, the system should gracefully handle success, error and loading states</li>
        <li>If an inline action fails, present an error message inline</li>
        <li>If an inline action succeeds, present a <a href="messages">success message</a> at the top of the screen</li>
        <li>If adding a row, show the new row at the top</li>
    </ul>

    <h3>Contextual examples</h3>
    <div class="example-thumbs">
        <a href="/images/aui/examples/tables/example-full-01.png" target="_blank"><img src="/images/aui/examples/tables/example-thumb-01.png" alt=""></a>
        <a href="/images/aui/examples/tables/example-full-02.png" target="_blank"><img src="/images/aui/examples/tables/example-thumb-02.png" alt=""></a>
        <a href="/images/aui/examples/tables/example-full-03.png" target="_blank"><img src="/images/aui/examples/tables/example-thumb-03.png" alt=""></a>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/tables.html" target="_blank">AUI Documentation</a> – implementation details</li>
        <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=tables" target="_blank">AUI Sandbox</a> – code snippets</li>
    </ul>

    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection