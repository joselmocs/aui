@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/aui/tables.css") }}
{{ HTML::style("/styles/aui/overrides/aui-group.css") }}
{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Controls</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
    <div class="aui-page-panel-inner">
    <section class="aui-page-panel-content">
    <h2>Buttons</h2>

    <p>Buttons are used as triggers for actions. They are used in <a href="forms.html">forms</a>, <a href="toolbar.html">toolbars</a>, <a href="modal-dialog.html">dialog</a> footers and as stand-alone action triggers. Buttons should rarely, if ever, be used for navigation.</p>
    <p><strong>Actions</strong> are operations that are performed on objects.</p>
    <p><strong>Navigation</strong> changes the screen or view or takes you to another context in the application.</p>

    <h3>Button types</h3>
    <p>Button styles can be applied to <span class="aui-lozenge aui-lozenge-code">&lt;a&gt;</span> and <span class="aui-lozenge aui-lozenge-code">&lt;button&gt;</span> elements.</p>

    <table class="aui">
        <thead>
        <tr>
            <th id="basic-button" style="min-width: 150px">Button</th>
            <th id="basic-description">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td headers="basic-button"><button class="aui-button aui-button-primary">Primary button</button></td>
            <td headers="basic-description">To be used as the happy path action on forms. If a strong call to action is needed on a page then this can also be used. Only ever appears once per screen (can appear in a modal dialog as the underlying page is effectively disabled).</td>
        </tr>
        <tr>
            <td headers="basic-button"><button class="aui-button">Standard button</button></td>
            <td headers="basic-description">The generic button for actions on a page. Never use this for a cancel or reset action in a form, instead use the link button style.</td>
        </tr>
        <tr>
            <td headers="basic-button"><button class="aui-button aui-button-link">Link button</button></td>
            <td headers="basic-description">A less visually strong, or supporting action. Used for secondary actions, or destructive actions such as "Cancel".</td>
        </tr>
        <tr>
            <td headers="basic-button">
                <button class="aui-button aui-dropdown2-trigger" href="#dropdown2-more" aria-owns="dropdown2-more" aria-haspopup="true" aria-controls="dropdown2-more">Dropdown button</button>
                <div id="dropdown2-more" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">Dropdown buttons trigger menus with a set of related actions. The actions in the menu should be contextual to the button and be useful given the current task the user is performing. Menus that are too long for the available space should scroll so all options can be accessed.</td>
        </tr>
        <tr>
            <td headers="basic-button">
                <div id="split-container" class="aui-buttons">
                    <button class="aui-button aui-button-split-main">Split button</button>
                    <button class="aui-button aui-dropdown2-trigger aui-button-split-more" href="#dropdown-button-split" data-container="#split-container" aria-owns="dropdown-button-split" aria-haspopup="true">Split More</button>
                </div>
                <div id="dropdown-button-split" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">
                <p>A button with an attached dropdown menu of related actions. The button triggers an action, and the attached dropdown menu shows a list of related actions (including the one shown on the button).</p>
            </td>
        </tr>
        <tr>
            <td headers="basic-button"><button class="aui-button aui-button-subtle"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span> Subtle button</button></td>
            <td headers="basic-description">A button that triggers actions that apply to an entity, such as a Confluence page or the JIRA issue navigator. This button is used when a toolbar or standard buttons would otherwise draw attention away from more important content. This button is often used at the top of a page or section.  An icon should be used with this button.</td>
        </tr>
        </tbody>
    </table>

    <h3>Button variations and examples</h3>

    <table class="aui">
        <thead>
        <tr>
            <th id="basic-button" style="width: 300px">Button</th>
            <th id="basic-description">Description</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td headers="basic-button"><button class="aui-button" aria-pressed="true">Pressed toggle button</button></td>
            <td headers="basic-description">Toggle buttons have a normal and pressed state and can be used for filter buttons. The pressed state can be enabled by using the aria attribute <span class="aui-lozenge aui-lozenge-code">&lt;aria-pressed="true"&gt;</span>.</td>
        </tr>

        <tr>
            <td headers="basic-button"><button class="aui-button" aria-disabled="true">Disabled button</button></td>
            <td headers="basic-description">Disabled states are used for actions that are temporarily unavailable by using the aria attribute <span class="aui-lozenge aui-lozenge-code">&lt;aria-disabled="true"&gt;</span>.</td>
        </tr>

        <tr>
            <td headers="basic-button"><button class="aui-button"><span class="aui-icon aui-icon-small aui-iconfont-configure"></span> Standard button with icon</button></td>
            <td headers="basic-description">Icons may be used in standard buttons to help communicate meaning.</td>
        </tr>

        <tr>
            <td headers="basic-button">
                <button class="aui-button aui-dropdown2-trigger aui-button-icon" href="#dropdown-button3" aria-owns="dropdown-button3" aria-haspopup="true" aria-controls="dropdown-button3"><span class="aui-icon aui-icon-small aui-iconfont-configure"></span> Dropdown button with icon</button>
                <div id="dropdown-button3" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">Dropdown buttons can be used with icons.</td>
        </tr>

        <tr>
            <td headers="basic-button"><button class="aui-button"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button></td>
            <td headers="basic-description">Icon only button. Use when space is constrained and the function of the button is obvious. Using a label is preferred over icon only.</td>
        </tr>

        <tr>
            <td headers="basic-button">
                <button class="aui-button aui-dropdown2-trigger" href="#dropdown-button2" aria-owns="dropdown-button5" aria-haspopup="true" aria-controls="dropdown-button5"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>

                <div id="dropdown-button5" class="aui-dropdown2 aui-style-default" style="display: none; top: 1073px; left: 30px;" aria-hidden="true" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div></td>
            <td headers="basic-description">Icon only dropdown button. Use when space is at a premium and the function of the button is obvious. Using a label is preferred over icon only.</td>
        </tr>

        <tr>
            <td headers="basic-button">
                <button class="aui-button aui-button-subtle aui-dropdown2-trigger" href="#dropdown-button1" aria-owns="dropdown-button1" aria-haspopup="true" aria-controls="dropdown-button1"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span> Subtle button with dropdown</button>
                <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">Entity action buttons can trigger dropdowns.</td>
        </tr>

        <tr>
            <td headers="basic-button">
                <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-subtle aui-dropdown2-trigger"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">When space is constrained, an entity action button can be used without a label (icon only).</td>
        </tr>
        </tbody>
    </table>

    <h3>Button special cases and examples</h3>

    <table class="aui">
        <thead>
        <tr>
            <th id="basic-button" style="width: 180px">Button</th>
            <th id="basic-description">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td headers="basic-button" class="aui-compact-button-column">
                <button class="aui-button aui-button-compact">Compact button</button>
                <button aria-owns="dropdown-button1" aria-haspopup="true" class="aui-button aui-button-compact aui-button-subtle aui-dropdown2-trigger"><span class="aui-icon aui-icon-small aui-iconfont-configure">Configure</span></button>
                <div id="dropdown-button1" class="aui-dropdown2 aui-style-default" style="display: none; top: 0px; min-width: 160px; left: 468px;" aria-hidden="false" data-dropdown2-alignment="left">
                    <ul class="aui-list-truncate">
                        <li><a href="#" class="active">Menu item 1</a></li>
                        <li><a href="#" class="">Menu item 2</a></li>
                        <li><a href="#" class="">Menu item 3</a></li>
                    </ul>
                </div>
            </td>
            <td headers="basic-description">This button style is mainly used for the 'cog menu' in <a href="tables.html">tables</a>. Also, when a button or a row of buttons are placed within a container (visible or invisible) that is too strictly constrained for the buttons to have enough space, compact buttons can be used. This rule is only to be applied when all other options (merging into dropdown buttons, cutting down actions, rewording button labels etc.) have been exhausted.</td>
        </tr>
        </tbody>
    </table>


    <h3>When and how to use this pattern</h3>
    <ul>
        <li><strong>Standalone</strong> – when buttons appear by themselves use a standard button</li>
        <li><strong>Groups</strong> – buttons can appear in groups for a set of related actions. See <a href="toolbar.html">Toolbar buttons</a> for more details</li>
        <li><strong>Dropdown buttons</strong> – secondary actions that are related can be grouped into a dropdown menu, which can save space</li>
        <li><strong>Icons</strong> – use icons where needed. Refer to the <a href="iconography.html">Iconography and avatars</a> page for more details</li>
        <li><strong>Keyboard equivalents</strong> – consider whether your action is important enough for a keyboard shortcut</li>
        <li><strong>Button titles</strong> – use short sentence case titles on buttons to indicate the action</li>
    </ul>

    <h3>What happens if …</h3>
    <ul>
        <li><strong>… I need an element that, when clicked, goes to another page or area of a different context:</strong> In most cases simple navigation links should suffice</li>
        <li><strong>… I have two buttons on a screen or dialog, neither of which are the  most important action for the user. Which one should be the primary?</strong> A primary button is not necessary on every screen (although it should be obvious to the user what they should do next)</li>
        <li><strong>… I have a small amount of available space and I need several buttons next to each other. Can I drop the labels?</strong> As in the examples above, you can use icon only buttons and icon only entity action buttons, however these should be used sparingly and where the icon is reasonably obvious (for example the cog or add icons). As mentioned in the above description, using a label and icon is preferred over using an icon alone</li>
    </ul>

    <h3>Usage</h3>

    <h4>Do</h4>
    <ul>
        <li><strong>Standalone</strong> – when buttons appear by themselves use a standard button</li>
        <li><strong>Groups</strong> – buttons can appear in groups for a set of related actions. See <a href="toolbar.html">Toolbar buttons</a> for more details</li>
        <li><strong>Dropdown buttons</strong> – only include useful actions in a dropdown menu, don't hide unused actions from the user just to unclutter the UI.</li>
        <li><strong>Icons</strong> – use icons where needed. Refer to the <a href="iconography.html">Iconography</a> and <a href="avatars.html">Avatars</a> page for more details</li>
        <li><strong>Keyboard equivalents</strong> – consider whether your action is important enough for a <a href="keyboard-shortcuts.html">keyboard shortcut</a></li>
        <li><strong>Button titles</strong> – use short sentence case titles on buttons to indicate the action</li>
    </ul>

    <h4>Don't</h4>
    <ul>
        <li>Use buttons for navigation links</li>
        <li>Use buttons in place of checkboxes or radio buttons in forms</li>
        <li>Use to display inactive text or page headers</li>
    </ul>

    <h3>Contextual examples</h3>
    <div class="example-thumbs">
        <a href="/images/aui/examples/buttons/example-full-01.png" target="_blank"><img src="/images/aui/examples/buttons/example-thumb-01.png" alt=""></a>
        <a href="/images/aui/examples/buttons/example-full-02.png" target="_blank"><img src="/images/aui/examples/buttons/example-thumb-02.png" alt=""></a>
        <a href="/images/aui/examples/buttons/example-full-03.png" target="_blank"><img src="/images/aui/examples/buttons/example-thumb-03.png" alt=""></a>
        <a href="/images/aui/examples/buttons/example-full-04.png" target="_blank"><img src="/images/aui/examples/buttons/example-thumb-04.png" alt=""></a>
    </div>

    <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
    <ul>
        <li><a href="http://docs.atlassian.com/aui/5.2/docs/buttons.html" target="_blank">AUI Documentation</a> – implementation details</li>
        <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=buttons" target="_blank">AUI Sandbox</a> – code snippets</li>
    </ul>
    </section><!-- .aui-page-panel-content -->
    </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection