@extends("template-base")
@include("aui/template-header")
@include("aui/template-footer")

@section("styles")@parent
{{ HTML::style("/styles/aui/aui-page-header.css") }}
{{ HTML::style("/styles/aui/tables.css") }}
{{ HTML::style("/styles/aui/tabs.css") }}
{{ HTML::style("/styles/aui/overrides/aui-group.css") }}
{{ HTML::style("/styles/apps/aui/examples.css") }}
@endsection

@section("aui-page-type")aui-layout aui-theme-default @endsection

@section("javascript-cdata")@parent
require(["libs/ajs/tabs"]);
@endsection

@section("content")
<section id="content">

    <header class="aui-page-header">
        <div class="aui-page-header">
            <div class="aui-page-header-inner">
                <h1>Controls</h1>
            </div>
        </div>
    </header>

    <div class="aui-page-panel">
        <div class="aui-page-panel-inner">
            <section class="aui-page-panel-content">
                <h2>Tabs</h2>

                <p>Tabs are used for alternating between views within the same context, not to navigate to different areas. For navigation refer to the <a href="horizontal-navigation.html">horizontal navigation</a> guidelines.</p>

                <h3>Interactive example</h3>

                <div class="example-container">
                    <h2>Atlassian staff</h2>
                    <div class="aui-tabs horizontal-tabs" id="tabs-example1" role="application">
                        <ul class="tabs-menu" role="tablist">
                            <li class="menu-item active-tab" role="presentation">
                                <a href="#tabs-example-first" id="aui-uid-0" role="tab" aria-selected="true"><strong>Designers</strong></a>
                            </li>
                            <li class="menu-item" role="presentation">
                                <a href="#tabs-example-second" id="aui-uid-1" role="tab" aria-selected="false"><strong>Developers</strong></a>
                            </li>
                            <li class="menu-item" role="presentation">
                                <a href="#tabs-example-third" id="aui-uid-2" role="tab" aria-selected="false"><strong>PMs</strong></a>
                            </li>
                        </ul>
                        <div class="tabs-pane active-pane" id="tabs-example-first" role="tabpanel" aria-hidden="false" aria-labelledby="aui-uid-0">
                            <h3>Designers</h3>
                            <table class="aui aui-table-interactive">
                                <thead>
                                <tr>
                                    <th id="basic-number">#</th>
                                    <th id="basic-fname">First name</th>
                                    <th id="basic-lname">Last name</th>
                                    <th id="basic-username">Username</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td headers="basic-number">1</td>
                                    <td headers="basic-fname">Matt</td>
                                    <td headers="basic-lname">Bond</td>
                                    <td headers="basic-username">mbond</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">2</td>
                                    <td headers="basic-fname">Ross</td>
                                    <td headers="basic-lname">Chaldecott</td>
                                    <td headers="basic-username">rchaldecott</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">3</td>
                                    <td headers="basic-fname">Henry</td>
                                    <td headers="basic-lname">Tapia</td>
                                    <td headers="basic-username">htapia</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tabs-pane" id="tabs-example-second" role="tabpanel" aria-hidden="true" aria-labelledby="aui-uid-1">
                            <h3>Developers</h3>
                            <table class="aui aui-table-interactive">
                                <thead>
                                <tr>
                                    <th id="basic-number">#</th>
                                    <th id="basic-fname">First name</th>
                                    <th id="basic-lname">Last name</th>
                                    <th id="basic-username">Username</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td headers="basic-number">4</td>
                                    <td headers="basic-fname">Seb</td>
                                    <td headers="basic-lname">Ruiz</td>
                                    <td headers="basic-username">sruiz</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">7</td>
                                    <td headers="basic-fname">Sean</td>
                                    <td headers="basic-lname">Curtis</td>
                                    <td headers="basic-username">scurtis</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">8</td>
                                    <td headers="basic-fname">Matthew</td>
                                    <td headers="basic-lname">Watson</td>
                                    <td headers="basic-username">mwatson</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tabs-pane" id="tabs-example-third" role="tabpanel" aria-hidden="true" aria-labelledby="aui-uid-2">
                            <h3>Product management</h3>
                            <table class="aui aui-table-interactive">
                                <thead>
                                <tr>
                                    <th id="basic-number">#</th>
                                    <th id="basic-fname">First name</th>
                                    <th id="basic-lname">Last name</th>
                                    <th id="basic-username">Username</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td headers="basic-number">5</td>
                                    <td headers="basic-fname">Jens</td>
                                    <td headers="basic-lname">Schumacher</td>
                                    <td headers="basic-username">jschumacher</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">6</td>
                                    <td headers="basic-fname">Sten</td>
                                    <td headers="basic-lname">Pittet</td>
                                    <td headers="basic-username">spittet</td>
                                </tr>
                                <tr>
                                    <td headers="basic-number">9</td>
                                    <td headers="basic-fname">James</td>
                                    <td headers="basic-lname">Dumay</td>
                                    <td headers="basic-username">jdumay</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- .aui-tabs -->
                </div>

                <h3>Usage</h3>
                <ul>
                    <li>The currently selected tab is always highlighted and is connected to the content area</li>
                    <li>The unselected tabs are clearly visible but are faded in comparison</li>
                </ul>

                <h4>Do</h4>
                <ul>
                    <li>Use tabs to alternate between view within the same context</li>
                    <li>Use short labels on tabs to describe the content view</li>
                    <li><a href="http://en.wikipedia.org/wiki/Letter_case#Usage" target="_blank">Sentence case</a> should always be used on tab items</li>
                </ul>

                <h4>Don't</h4>
                <ul>
                    <li>Use tabs for navigating between pages</li>
                    <li>Use tabs across multiple rows</li>
                    <li>Put a dropdown menu in a tab item, this is not navigation</li>
                </ul>

                <h3>Contextual examples</h3>
                <div class="example-thumbs">
                    <a href="/images/aui/examples/tabs/example-full-01.png" target="_blank"><img src="/images/aui/examples/tabs/example-thumb-01.png" alt=""></a>
                    <a href="/images/aui/examples/tabs/example-full-02.png" target="_blank"><img src="/images/aui/examples/tabs/example-thumb-02.png" alt=""></a>
                    <a href="/images/aui/examples/tabs/example-full-03.png" target="_blank"><img src="/images/aui/examples/tabs/example-thumb-03.png" alt=""></a>
                </div>

                <h3>Code and documentation <span class="aui-lozenge aui-lozenge-success aui-lozenge-inline">AVAILABLE IN AUI</span></h3>
                <ul>
                    <li><a href="http://docs.atlassian.com/aui/5.2/docs/tabs.html" target="_blank">AUI Documentation</a> – implementation details</li>
                    <li><a href="http://docs.atlassian.com/aui/5.2/sandbox/?component=tabs" target="_blank">AUI Sandbox</a> – code snippets</li>
                </ul>
            </section><!-- .aui-page-panel-content -->
        </div><!-- .aui-page-panel-inner -->
    </div>

</section>
@endsection