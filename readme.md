## AUI Project (Laravel PHP Framework + Atlassian AUI)

## Laravel PHP Framework
Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.
[Laravel website](http://laravel.com/) [Github](https://github.com/laravel/laravel)


## Atlassian User Interface (AUI) Library
Atlassian provides a toolkit for you to help you create beautiful features for our products. The toolkit combines the Atlassian Design Guidelines (ADG) and the Atlassian User Interface Library (AUI). The two work harmoniously together to provide a complete toolkit for you.
[AUI website](https://developer.atlassian.com/design/latest/index.html) [Github](https://bitbucket.org/atlassian/aui)
